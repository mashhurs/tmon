<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Users;
use app\models\Groups;
use app\models\Mobiles;
use app\models\Products;
use app\models\Orders;
use app\models\Emotions;
use app\models\FileAttachment;
use yii\web\UploadedFile;
use app\models\Sessions;
use app\models\Files;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

class ApiController extends Controller
{
    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to
                    'Origin' => ['*'],
                    //'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                    // Allow only POST and PUT methods
                    // 'Access-Control-Request-Headers' => ['X-Wsse'],
                    // // Allow only headers 'X-Wsse'
                    // 'Access-Control-Allow-Credentials' => true,
                    // // Allow OPTIONS caching
                    // 'Access-Control-Max-Age' => 3600,
                    // // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    // 'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],

            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    private $data = null;

    public function init() {
       
        parent::init();
        \Yii::$app->user->enableSession = false;
        $this->enableCsrfValidation = false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$p = Yii::$app->request->getBodyParams();
		$g = Yii::$app->request->getQueryParams();
        $this->data = array_merge($p, $g);

    }

    public function actionCheck()
    {
    /* Проверка ключа
    {"id":"123", "token":"h27b8h2jbkjbak3k", ...}
    http://127.0.0.1:8189/index.php?r=api/check&id=4&token=HEGijbLffxi1DvuBQ-L-OFg517Xm_X-n
    */
        if ($this->data && isset($this->data['token']) && strlen($this->data['token']) > 0) {
            if (!isset($this->data['id']) || !isset($this->data['token'])) return ['status' => false, 'message' => 'empty required parameters'];
            $user = Users::findOne(['user_id' => $this->data['id'], 'auth_key' => $this->data['token']]);
            if ($user) {
                return ['status' => true, 'id' => $user->user_id, 'role' => $user->user_role, 'user_nick' => $user->user_nick];
            }
        }
        return ['status' => false];
    }

    public function actionLogin()
    {
    /* Авторизация
    {"user_login":"customer", "user_pass":"321654"}
    http://192.168.88.172:8080/index.php?r=api/login&user_login=customer&user_pass=321654
    */
        if ($this->data) {
            if (!isset($this->data['user_login']) || !isset($this->data['user_pass'])) return ['status' => false, 'message' => 'empty required parameters'];
            $user = Users::findOne(['user_login' => $this->data['user_login'], 'user_role' => ['customer']]);
            if ($user && $user->validatePassword($this->data['user_pass'])) {
                if (strlen($user->auth_key) <= 0) {
                    $user->auth_key = Yii::$app->security->generateRandomString();
                    $user->save(true, array('auth_key'));
                }

                if (isset($this->data['device_type']) && isset($this->data['device_token']) && in_array($this->data['device_type'], array('android', 'ios'))) {
                    // Сохраняем мобильный токен 
                    $mobile = Mobiles::find()->where(['id_user' => $user->user_id, 'device_token' => $this->data['device_token'], 'device_type' => $this->data['device_type']])->one();
                    if (!$mobile) {
                        $mobile = new Mobiles();
                        $mobile->id_user = $user->user_id;
                        $mobile->device_type = $this->data['device_type'];
                        $mobile->device_token = $this->data['device_token'];
                        $mobile->save();
                    }
                }
                return ['status' => true, 'id' => $user->user_id, 'token' => $user->auth_key, 'role' => $user->user_role, 'user_nick' => $user->user_nick];
            }
        }
        return ['status' => false];
    }

    public function actionListProducts()
    {
    /* Отдаем  список товаров
    {..., ["id":"6"]}
    http://192.168.88.172:8080/index.php?r=api/list-products&id=6&token=VYOuYOpgIcxfRYhpYdZ4xjUCRfvdITVG
    http://192.168.88.172:8080/index.php?r=api/list-products&id=6&token=VYOuYOpgIcxfRYhpYdZ4xjUCRfvdITVG&product_id=1
    */
       $userI = $this->actionCheck();

       if ($this->data && $userI['status']) {
           $offset = isset($this->data["offset"]) ? $this->data["offset"] + 0 : 0;
           $limit = (isset($this->data["limit"]) && $this->data["limit"] > 0) ? $this->data["limit"] + 0 : 50;

           $products = Products::find()->offset($offset)->limit($limit)->all();
           if (isset($this->data["product_id"]))  {
               $products = Products::find()
                    ->where(['id' => $this->data["product_id"]])->one();
           }
           if ($products) {
               $products = ArrayHelper::toArray($products);
               return ['status' => true, 'list' => $products];
           } else return ['status' => true, 'list' => []];
       } else return ['status' => false, 'message' => 'auth problem'];

        return ['status' => false];
    }

    public function actionListEmotions()
    {
    /* Отдаем  список emotions
    {..., ["id":"6"]}
    http://192.168.88.172:8080/index.php?r=api/list-emotions&id=6&token=GWNinwCCTVR_mcXBhKhN54esYp9VkDaR
    http://192.168.88.172:8080/index.php?r=api/list-emotions&id=6&token=VYOuYOpgIcxfRYhpYdZ4xjUCRfvdITVG&emotion_id=1
    */
       $userI = $this->actionCheck();

       if ($this->data && $userI['status']) {
           $offset = isset($this->data["offset"]) ? $this->data["offset"] + 0 : 0;
           $limit = (isset($this->data["limit"]) && $this->data["limit"] > 0) ? $this->data["limit"] + 0 : 50;

           $emotions = Emotions::find()->offset($offset)->limit($limit)->where(['emotion_state' => 'active'])->all();
           if (isset($this->data["emotion_id"]))  {
               $emotions = Emotions::find()
                    ->where(['emotion_id' => $this->data["emotion_id"]])->one();
           }
          
           if ($emotions) {
                $emotions = ArrayHelper::toArray($emotions);
               return ['status' => true, 'list' => $emotions];
           } else return ['status' => true, 'list' => []];
       } else return ['status' => false, 'message' => 'auth problem'];

        return ['status' => false];
    }


    public function actionListOrders()
    {
        /* Отдаем  список заказов
    {..., ["id":"6"]}
    http://192.168.88.172:8080/index.php?r=api/list-orders&id=6&token=-ro-tEOlRWbr1Z6Cv_eqg1-lQSbBiRqR
    http://192.168.88.172:8080/index.php?r=api/list-orders&id=6&token=-ro-tEOlRWbr1Z6Cv_eqg1-lQSbBiRqR&order_id=1

    */
       $userI = $this->actionCheck();
       //var_export($userI);return;

       if ($this->data && $userI['status']) {
           $offset = isset($this->data["offset"]) ? $this->data["offset"] + 0 : 0;
           $limit = (isset($this->data["limit"]) && $this->data["limit"] > 0) ? $this->data["limit"] + 0 : 50;

           $orders = Orders::find()
                    ->with('products')
                    ->where(['account_srl' => $userI['id']])->offset($offset)
                    ->limit($limit)->asArray()->all();
           if (isset($this->data["order_id"])) {
               $orders = Orders::find()
                    ->with('products')
                    ->where(['id' => $this->data["order_id"], 
                            'account_srl' => $userI['id']])->one();
           } 
           if ($orders) {
       // $orders = ArrayHelper::toArray($orders);
            //var_export($orders);
                
               return ['status' => true, 'list' => $orders];
           } else return ['status' => true, 'list' => []];
       } else return ['status' => false, 'message' => 'auth problem'];

        return ['status' => false];
    }
    

    /*
    * Not used
    */
    public function actionImage()
    {
    /* Скачиваем картинку
    {..., "main_deal_srl":"2001" }
    http://192.168.88.172:8080/index.php?r=api/image&id=6&token=VYOuYOpgIcxfRYhpYdZ4xjUCRfvdITVG&main_deal_srl=2001
    */
        $userI = $this->actionCheck();

        if ($this->data && $userI['status']) {

            if (!isset($this->data['main_deal_srl'])) return ['status' => false, 'message' => 'empty required parameters'];

            $product = Products::find()->where(['id' => $this->data['id'] ])->one();

            if ($product) {
                
                 $file_path = $product->getFilePath(); 
                 //return ['f'=> $file_path];
                if ($file_path && file_exists($file_path)) {

                    //return [ 'f' => $file_path . $product->img];
                    Yii::$app->response->SendFile($file_path . $product->img);
                }
            } else return ['status' => false, 'message' => 'wrong id request, or state'];
        } else return ['status' => false, 'message' => 'auth problem'];
    }

     public function actionFile()
    {
    /* Скачиваем картинку
    {..., "main_deal_srl":"2001" }
    http://timon.lc:8080/index.php?r=api/file&id=6&token=GWNinwCCTVR_mcXBhKhN54esYp9VkDaR&img_id=5
    */
        $userI = $this->actionCheck();
        if ($this->data && $userI['status']) {
            if (!isset($this->data['img_id'])) return ['status' => false, 'message' => 'empty required parameters'];
            $file = Files::findOne($this->data['img_id']);
            if ($file) {
                $file_path = $file->getFilePath(); 
                //var_dump($file_path. $file->filename);die;
                if ($file_path && file_exists($file_path . $file->file_url)) {
                      Yii::$app->response->SendFile($file_path . $file->file_url);
                 }
            } else return ['status' => false, 'message' => 'wrong id request, or state'];
            
        } else return ['status' => false, 'message' => 'auth problem'];
    }

//curl  -X POST -d 'id=3&token=g41jLJvdePUd05p5Jq8EohU9_ZsMFQnv' http://tmon.mss.uz/index.php?r=api/agents-list&id=3&token=g41jLJvdePUd05p5Jq8EohU9_ZsMFQnv
    //curl  -X POST -d 'id=4&token=dio-pC92zJsW446xNuvcjZ4BTnQTvHyQ' http://timon.lc:8080/index.php?r=api/agents-list&id=4&token=dio-pC92zJsW446xNuvcjZ4BTnQTvHyQ
    public function actionAgentsList()
    {
    /* Список агентов
    {"id":"4", "token":"dio-pC92zJsW446xNuvcjZ4BTnQTvHyQ"}
    http://timon.lc:8080/index.php?r=api/agents-list&id=3&token=-ro-tEOlRWbr1Z6Cv_eqg1-lQSbBiRqR
    */
         $userI = $this->actionCheck();

         if ($this->data && $userI['status']) {
            $offset = isset($this->data["offset"]) ? $this->data["offset"] + 0 : 0;
            $limit = (isset($this->data["limit"]) && $this->data["limit"] > 0) ? $this->data["limit"] + 0 : 50;

            $users = Users::find()->    where(['user_role' => 'agent'])
                        ->offset($offset)->limit($limit)->all();

            if ($users) {
                $users = ArrayHelper::toArray($users);
                return ['status' => true, 'list' => $users];
            } else return ['status' => true, 'list' => []];

         } else return ['status' => false, 'message' => 'auth problem'];

    }
 
    // curl  -X POST -d 'id=3&token=-ro-tEOlRWbr1Z6Cv_eqg1-lQSbBiRqR' http://timon.lc:8080/index.php?r=api/groups-list&id=3&token=g41jLJvdePUd05p5Jq8EohU9_ZsMFQnv
    public function actionGroupsList()
    {     
        /* Список групп
        {"id":"3", "token":"g41jLJvdePUd05p5Jq8EohU9_ZsMFQnv"}
        http://timon.lc:8080/index.php?r=api/groups-list&id=3&token=-ro-tEOlRWbr1Z6Cv_eqg1-lQSbBiRqR
        */  
         $userI = $this->actionCheck();

         if ($this->data && $userI['status']) {
            $offset = isset($this->data["offset"]) ? $this->data["offset"] + 0 : 0;
            $limit = (isset($this->data["limit"]) && $this->data["limit"] > 0) ? $this->data["limit"] + 0 : 50;

            $groups = Groups::find()
                        ->offset($offset)->limit($limit)->all();
  
            if ($groups) {
                $groups = ArrayHelper::toArray($groups);
                return ['status' => true, 'list' => $groups];
            } else return ['status' => true, 'list' => []];

         } else return ['status' => false, 'message' => 'auth problem'];
    }

    public function actionUpload() {
/* Заливаем картинку
form enctype="multipart/form-data", name="FileAttachment[files][]"
{..., "id_request":"1" [, "descript":"Descript file", "id_file":"1"]}
http://127.0.0.1:8189/index.php?r=api/upload&id=4&token=k89ApF3aY4kU2sCFvk4RUQkc7_FigdE2&id_request=1
*/
        $userI = $this->actionCheck();
        //var_export(Yii::$app->request->getHeaders());
        //var_export(Yii::$app->request->getRawBody());
        //var_export($this->data);
        //return ;
        if ($this->data && $userI['status']) {
            //if (!isset($this->data['files'])) return ['status' => false, 'message' => 'empty required parameters'];

//            $chat_session = Sessions::find()->where(['user_id' => $userI['id']])->one();
//            if ($chat_session) {
                
                $file = new Files();
                $file->user_id = $userI['id'];
                //$file->session_id = $chat_session->session_id;
                $file->file_title = "";
                $file->file_url = "";
                $file->file_type = "image";                
                $file->save();

                if ($file) $file_path = $file->getFilePath(); 
                if ($file_path) {
                    $attachment = new FileAttachment();
                    $attachment->files = UploadedFile::getInstances($attachment, 'files');
                    //var_export($attachment->files);
                    $file->file_url =  $file->file_id . '_' . $attachment->upload($file_path . $file->file_id . '_');
                    if ($file->file_url) {
                        $file_name = explode('.', $file->file_url);
                        $file->file_title = $file_name[0];                        
                        $file->save(['file_url', 'file_title']);
                        $url =  Url::base(true) . Url::toRoute(['site/image', 'img_id' => $file->file_id ]);
                        return ['status' => true, 'id_file' => $file->file_id, 'file_url' => $url ];
                    } else return ['status' => false, 'message' => 'file not valid'];
                } else return ['status' => false, 'message' => 'problem create record file'];
//            } else  return ['status' => false, 'message' => 'user chat not found']; 
        } else return ['status' => false, 'message' => 'auth problem'];
        return ['status' => false];
    }

    public function actionParse($url)
    {
        $html = file_get_contents($url);
        if (!$html) return ['status' => false];

        $title = preg_match("/<title>(.*)<\/title>/siU", $html, $title_matches);
        if ($title) {
            $title = preg_replace('/\s+/', ' ', $title_matches[1]);
            $title = trim($title);
        }

        libxml_use_internal_errors(true); // Yeah if you are so worried about using @ with warnings
        $doc = new \DomDocument();
        $doc->loadHTML($html);
        $xpath = new \DOMXPath($doc);
        $query = '//*/meta[starts-with(@property, \'og:\')]';
        $metas = $xpath->query($query);
        $rmetas = array();
        foreach ($metas as $meta) {
            $property = $meta->getAttribute('property');
            $content = $meta->getAttribute('content');
            $rmetas[$property] = $content;
        }

        return ['status' => true, 'title' => $title, 'metas' => $rmetas];
    }

    // public function actionImage($img_id)
    // {
    //     $userI = $this->actionCheck();

    //     if ($this->data && $userI['status']) {

    //     }

    //     Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    //     $file = Files::findOne($img_id);

    //     if ($file) $file_path = $file->getFilePath(); 
    //     //var_dump($file_path. $file->filename);die;
    //     if ($file_path && file_exists($file_path . $file->file_url)) {
    //         Yii::$app->response->SendFile($file_path . $file->file_url);
    //     }
    // }


    public function actionInfo()
    {
        phpinfo();
    }

}
