<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Users;
use app\models\Settings;
use app\models\Files;

class SiteController extends Controller
{
   

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => ['class' => 'app\components\AccessRule'],
                'rules' => [
                    [   
                        'actions' => ['index' , 'settings','image'],
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],
                    [   
                        'actions' => ['logout', 'index', 'image'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [   
                        'actions' => ['login'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {

        return $this->render('index');
    }

    public function actionSettings()
    {
        $data = Settings::find()->all();
        $post = Yii::$app->request->post();
        if ($post) { 
            unset($post['_csrf']);
            unset($post['save-button']);

            $when = array();
            foreach ($post as $code => $value) {
                $when[] = 'WHEN code = "' . $code . '" THEN "' . $value . '"';
            }
            $query = 'UPDATE settings SET value = CASE ' . implode(' ', $when) . ' ELSE code END';
            //echo $query; die;
            $command = Yii::$app->db->createCommand($query);

            if ($command->execute()) {
                Yii::$app->getSession()->setFlash('success', 'Settings saved'); 
            } else {
                Yii::$app->getSession()->setFlash('error', 'Settings not saved');
            }
            return $this->redirect(['site/settings']);           
        }
        return $this->render('settings', [
            'data' => $data
        ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }       

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) { 

        if (Yii::$app->user->identity->user_role == 'administrator') {
                //return $this->goHome();
                return $this->redirect(Yii::$app->getUser()->getReturnUrl());
            } else {              
                return $this->redirect(['/chat']);
            }
            // return $this->goBack();         
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }


    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionImage($img_id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $file = Files::findOne($img_id);

        if ($file) $file_path = $file->getFilePath(); 
        //var_dump($file_path. $file->filename);die;
        if ($file_path && file_exists($file_path . $file->file_url)) {
            Yii::$app->response->SendFile($file_path . $file->file_url);
        }
    }

   

}
