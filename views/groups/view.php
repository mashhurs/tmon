<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Select2;
use yii\helpers\ArrayHelper;
use app\models\Groupsusers;

/* @var $this yii\web\View */
/* @var $model app\models\Groups */

$this->title = $model->group_title;
$this->params['breadcrumbs'][] = ['label' => 'Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="groups-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->group_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->group_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table class="table table-striped table-bordered detail-view">
        <tbody>
            <tr>
                <th>Group ID</th>
                <td><?php echo $model->group_id ?></td>
            </tr>
            <tr>
                <th>Group Title</th>
                <td><?php echo $model->group_title ?></td>
            </tr>
            <tr>
                <th>Group Create</th>
                <td><?php echo $model->group_create ?></td>
            </tr>
            <tr>
                <th>Agents</th>
                <td><?php 
                //var_export($model->users)
                foreach ($model->users as $v) {
                    echo $v->user_nick . '<br>';
                }
                ?></td>
            </tr>
        </tbody>
    </table>

    <?php /* echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'group_id',
            'group_title',
            'group_create',
            [
                'attribute'=>'user_id',
                'value'=>$model->users[0]->user_nick,
                //'type'=>DetailView::INPUT_SELECT2, 
                'widgetOptions'=>[
                  'data'=>ArrayHelper::map(Groupsusers::find()->orderBy('user_id')->asArray()->all(), 'id', 'name'),
                ]
            ],
        ]
        ]); */ ?>

</div>
