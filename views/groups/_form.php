<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\Groups */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="groups-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'group_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'group_thumb')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'group_backup')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'group_capacity')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'group_create')->textInput() ?>

    <?php 
	$selectedUsers = [];
	foreach ($model->users as $u) {
		$selectedUsers[ $u->user_id ] = ['Selected'=>'selected'];
	}
	/*print '<pre>';
	print_r ($selectedUsers);*/
	?>

    <?= $form->field($model, 'users[]')->dropDownList(Helper::getAgentsList(),
                        ['prompt'=>'- Add agents -', 'multiple' => 'multiple',
                        'options' => $selectedUsers ]) ?> 

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>











