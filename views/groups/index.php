<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Groups';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="groups-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Groups', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'group_id',
            [
                'format' => 'raw', 
                'label' => 'Thumb',
                'value' => function ($data) { 
                    return Html::img($data->group_thumb, ['width' => '50px']);
                },
            ],
            'group_title',
            'group_capacity',
            'group_backup',
            [
                'format' => 'text', 
                'label' => 'Agents',
                'value' => function ($data) { 
			$sList = "";
			$aList = $data->getUsers()->select('user_login')->asArray()->all();
			foreach ($aList as $key => $value) {
				$sList .= $value["user_login"] . ", ";
			}
                    return $sList;
                },
            ],
            'group_create',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
