<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Settings';

Yii::$app->view->registerJs("
	
");
?>
<style>
.tbl td {
    padding:10px;
}
</style>
<div class="site-index">

    <div class="jumbotron">
    	<?php echo $this->render('/site/alerts') ?>

        <h2>Settings</h2>

		<p class="text-center">
	        <?php echo Html::beginForm(['site/settings']); ?>
	        <div class="row text-right">
	        	<?php if ($data) { ?>
	        	<table class="tbl">
	        		<?php foreach ($data as $v) { ?>
	        		<tr>
	        			<td><?php echo Html::encode($v->desc) ?></td>
	        			<td>
	        				<?php echo Html::input('input', Html::encode($v->code), Html::encode($v->value)); ?>
	        			</td>
	        		</tr>
	        		<?php } ?>
	        		<tr>
	        			<td></td>
	        			<td>
	        				<?php echo Html::submitButton('Save', 
				                ['class' => 'btn btn-primary', 'name' => 'save-button', 
				                 'style' => 'font-size: 14px;padding: 6px 12px;']) ?> 
	        			</td>
	        		</tr>
	        	</table>
	        	<?php } ?>    
	          
	        </div>
	        <?php echo Html::endForm(); ?>
		</p>
    </div>
</div>