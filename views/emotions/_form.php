<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Emotions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="emotions-form">

    <?php $form = \yii\bootstrap\ActiveForm::begin([
        'enableClientValidation' => false,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>

    <?= $form->field($model, 'emotion_key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'emotion_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'emotion_url')->fileInput(['maxlength' => true]) ?>

    <img width="100px" src="<?php echo $model->getImageFileUrl('emotion_url') ?>" >

    <?= $form->field($model, 'emotion_state')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive']) ?>

    <?= $form->field($model, 'emotion_create')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php \yii\bootstrap\ActiveForm::end(); ?>

</div>
