<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmotionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="emotions-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'emotion_id') ?>

    <?= $form->field($model, 'emotion_key') ?>

    <?= $form->field($model, 'emotion_title') ?>

    <?= $form->field($model, 'emotion_url') ?>

    <?= $form->field($model, 'emotion_state') ?>

    <?php // echo $form->field($model, 'emotion_create') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
