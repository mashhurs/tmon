<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
/* @var $this yii\web\View */

$type = trim(Yii::$app->request->get('type'));  
$this->title = 'Agents';
?>
<div class="site-index">
    <div class="jumbotron">
        <h2>Agents</h2>

        <p class="text-right">            
            <a href="<?php echo Url::toRoute(['users/create', 'type' => 'agent']) ?>">Add agent</a>
        </p>

        <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>"{items}\n{pager}",
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'user_id',
                'label'=>'№ ID',                
            ], 
            [
                'attribute'=>'user_nick',
                'label'=>'Nick name',                
            ],
            [
                'attribute'=>'user_login',
                'label'=>'Login',                
            ],
            [
                'attribute'=>'user_capacity',
                'label'=>'Capacity',                
            ],
            [
	            'attribute' => 'user_create',
		    	'label' => 'Created at',
		    	'format' => ['date', 'php:d.m.Y']
            ],            
            [
	           	'attribute'=>'user_state',
		    	'label'=>'State',
		    	'format' => 'text',
		    	'content' => function($data) {
					return $data->getStatus($data->user_state);
		    	},
           ],           
           [
            'class' => 'yii\grid\ActionColumn',
            'header'=>'Actions', 
            'headerOptions' => ['width' => '80'],
            'template' => '{update} {delete}{link}',
            ],
        ],
    ]); ?>


    </div>
</div>
