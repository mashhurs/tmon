<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use dosamigos\datepicker\DatePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */


  $this->title = 'Add user';

if ($model->id) {
    $this->title .= ' ' . $model->user_nick;
} 
?>
<div class="site-index">
 <div class="jumbotron">
    <h3><?php echo Html::encode($this->title) ?></h3>
    <br><br>

    <div class="users-form">

        <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

        <div class="row">
          <div class="col-md-6">
         
          <?php echo $form->field($model, 'user_nick',[
                'horizontalCssClasses' => [
                    'wrapper' => 'col-sm-6',
                ]
            ])->textInput(['maxlength' => true]) ?>

             <?php /*echo $form->field($model, 'user_role',[
                'horizontalCssClasses' => [
                    'wrapper' => 'col-sm-6',
                ]
            ])->dropDownList([ 'agent' => 'Agent', 'customer' => 'Client' ], ['prompt' => '']) */ ?>
        
          
         
          
         
          <?php /*echo $form->field($model, 'user_create')->widget(
                            DatePicker::className(), [                               
                                'language' => 'ru',
                                 'attribute' => 'date',
                                'template' => '{addon}{input}',
                                    'clientOptions' => [
                                        'autoclose' => true,
                                        'format' => 'dd/m/yyyy'
                                    ]
                        ]); */ ?>
       
          </div>
          <div class="col-md-6">
             
              <?php echo $form->field($model, 'user_state',[
                'horizontalCssClasses' => [
                    'wrapper' => 'col-sm-6',
                ]
            ])->dropDownList([ 'Training' => 'Training', 'Lunch' => 'Lunch', 'O/B' => 'O/B', 'Online' => 'Online' ], []) ?>

          </div>
          <div class="col-md-6">
             
              <?php echo $form->field($model, 'user_capacity',[
                'horizontalCssClasses' => [
                    'wrapper' => 'col-sm-6',
                ]
            ])->textInput(['maxlength' => true]) ?>

          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
              <?php echo $form->field($model, 'user_login',[
                'horizontalCssClasses' => [
                    'wrapper' => 'col-sm-6',
                ]
            ])->textInput(['maxlength' => true, 'disabled' => !$model->isNewRecord]) ?>
              <?php echo $form->field($model, 'user_pass',[
                'horizontalCssClasses' => [
                    'wrapper' => 'col-sm-6',
                ]
            ])->passwordInput(['maxlength' => true]) ?>
              <?php echo $form->field($model, 'confirm_password',[
                'horizontalCssClasses' => [
                    'wrapper' => 'col-sm-6',
                ]
            ])->passwordInput() ?>
            </div>
             

         

            <div class="row">
                <div class="col-md-6">
       
                </div>
                <div class="col-md-6">
                     <br><br>
                     <div class="form-group">
                        <?php echo Html::submitButton('Save', [
                            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 
                            'style' => 'font-size: 14px;'] ) ?>
                    </div>
                </div>
            </div> 
         
        <?php ActiveForm::end(); ?>

    </div>
    </div>
  </div>
</div>