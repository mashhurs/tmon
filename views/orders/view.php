<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */

$this->title = 'Order №' . $model->main_buy_srl;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'main_buy_srl',
               // 'main_deal_srl',
                'date',
                'count',
                'state',
                'original_price',
                'discount_price',
                'coupon',
                'delivery_addr',
                'account_srl',
            ],
        ]) ?>
    </div>

    <div >
        <h3>Products</h3>
    </div>
    <div >
        <?php       
        if ($model->products) { 
            $productsCnt = count($model->products); 
                for ($i = 0; $i<$productsCnt; $i++) { ?>

                    <table class="table table-striped table-bordered detail-view">
                    <tbody>
                        <tr>
                            <th>Product ID</th>
                            <td><?php echo $model->products[$i]->main_deal_srl?></td>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <td><?php echo $model->products[$i]->name ?></td>
                        </tr>
                        <tr>
                            <th>Product description</th>
                            <td><?php echo $model->products[$i]->description ?></td>
                        </tr>
                         <tr>
                            <th>Product delivery</th>
                            <td><?php echo $model->products[$i]->delivery ?></td>
                        </tr>
                        <tr>
                            <th>Product price</th>
                            <td><?php echo $model->products[$i]->price ?></td>
                        </tr>
                        <tr>
                            <th>Category</th>
                            <td><?php echo $model->products[$i]->category_srl ?></td>
                        </tr>
                        <tr>
                            <th>Image</th>
                            <td><img width="100px" src="<?php echo $model->products[$i]->getImageFileUrl('img')?>" /></td>
                        </tr>
                        </tbody>
                    </table>
                <?php } ?>
        <?php }
         ?>
        <?php /*<table class="table table-striped table-bordered detail-view">
            <tbody>
                <tr>
                    <th>Product ID</th>
                    <td><?php echo $model->product->main_deal_srl ?></td>
                </tr>
                <tr>
                    <th>Name</th>
                    <td><?php echo $model->product->name ?></td>
                </tr>
                <tr>
                    <th>Product description</th>
                    <td><?php echo $model->product->description ?></td>
                </tr>
                 <tr>
                    <th>Product delivery</th>
                    <td><?php echo $model->product->delivery ?></td>
                </tr>
                <tr>
                    <th>Product price</th>
                    <td><?php echo $model->product->price ?></td>
                </tr>
                <tr>
                    <th>Category</th>
                    <td><?php echo $model->product->category_srl ?></td>
                </tr>
            </tbody>
        </table>*/?>
    </div>

</div>
