<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Rules */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rules-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'group_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rule_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rule_state')->dropDownList([ 'disabled' => 'Disabled', 'enabled' => 'Enabled']) ?>

    <?= $form->field($model, 'rule_priority')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rule_info')->textarea(['rows' => 6, 'placeholder' => '{"type":"OR","conditions":[{"key":"message_text", "value":"example", "strict":"complete"}, {"key":"message_text", "value":"test", "strict":"overlap"}]}']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
