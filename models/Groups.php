<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "groups".
 *
 * @property integer $group_id
 * @property string $group_title
 * @property string $group_create
 */
class Groups extends \yii\db\ActiveRecord
{
    public $agents;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'groups';
    }

    public function behaviors()
    {
        return [
            \cornernote\linkall\LinkAllBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_thumb'], 'string'],
            [['group_backup', 'group_capacity'], 'integer'],
            [['group_create'], 'safe'],
            [['group_title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'group_id' => 'Group ID',
            'group_thumb' => 'Group Thumb',
            'group_title' => 'Group Title',
            'group_backup' => 'Group Backup ID',
            'group_capacity' => 'Group Capacity',
            'group_create' => 'Group Create',
        ];
    }

    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['user_id' => 'user_id'])
            ->viaTable('groupsusers', ['group_id' => 'group_id']);
    }

    // public function getAgentsList()
    // {
    //     $users = \app\models\Users::find()
    //                     ->select(['user_id', 'user_nick'])
    //                     ->where(['user_role' => 'agent', 'user_state' => 'active'])
    //                     ->all();
    //     $agents = ArrayHelper::map($users, 'user_id', 'user_nick');

    //     Yii::info($agents, 'requests');

    //     return $agents;

    // }
}
