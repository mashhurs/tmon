<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sessions".
 *
 * @property integer $session_id
 * @property integer $user_id
 * @property string $session_state
 * @property string $session_update
 * @property string $session_create
 */
class Sessions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sessions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['session_state'], 'string'],
            [['session_update', 'session_create'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'session_id' => 'Session ID',
            'user_id' => 'User ID',
            'session_state' => 'Session State',
            'session_update' => 'Session Update',
            'session_create' => 'Session Create',
        ];
    }
}
