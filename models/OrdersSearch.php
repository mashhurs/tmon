<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Orders;

/**
 * OrdersSearch represents the model behind the search form about `app\models\Orders`.
 */
class OrdersSearch extends Orders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'main_buy_srl',  'count', 'account_srl'], 'integer'],
            [['date', 'state', 'coupon', 'delivery_addr'], 'safe'],
            [['original_price', 'discount_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'main_buy_srl' => $this->main_buy_srl,
            //'main_deal_srl' => $this->main_deal_srl,
            'date' => $this->date,
            'count' => $this->count,
            'original_price' => $this->original_price,
            'discount_price' => $this->discount_price,
            'account_srl' => $this->account_srl,
        ]);

        $query->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'coupon', $this->coupon])
            ->andFilterWhere(['like', 'delivery_addr', $this->delivery_addr]);

        return $dataProvider;
    }
}
