<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "files".
 *
 * @property integer $file_id
 * @property integer $user_id
 * @property string $file_title
 * @property string $file_url
 * @property string $file_type
 * @property string $file_create
 */
class Files extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['user_id'], 'required'],
            [['file_url', 'file_type'], 'string'],
            [['file_create'], 'safe'],
            [['file_title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file_id' => 'File ID',
            'user_id' => 'User ID',
            'file_title' => 'File Title',
            'file_url' => 'File Url',
            'file_type' => 'File Type',
            'file_create' => 'File Create',
        ];
    }

    public function getFilePath() {
        if (!isset($this->file_id) || !isset($this->user_id)) return null;

        $dir_f = Yii::getAlias(Yii::$app->params['uploadFile'] . $this->user_id . "/");
        if (is_dir($dir_f) || mkdir($dir_f, 0755, true)) {
            return $dir_f;
        }
        return null;
    }
}
