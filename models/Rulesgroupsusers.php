<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rulesgroupsusers".
 *
 * @property integer $rulegroupuser_id
 * @property integer $rule_id
 * @property integer $user_id
 * @property integer $group_id
 * @property string $rulegroupuser_create
 */
class Rulesgroupsusers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rulesgroupsusers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rule_id'], 'required'],
            [['rule_id', 'user_id', 'group_id'], 'integer'],
            [['rulegroupuser_create'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rulegroupuser_id' => 'Rulegroupuser ID',
            'rule_id' => 'Rule ID',
            'user_id' => 'User ID',
            'group_id' => 'Group ID',
            'rulegroupuser_create' => 'Rulegroupuser Create',
        ];
    }

    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['user_id' => 'user_id']); 
    }

    public function getRules()
    {
        return $this->hasMany(Rules::className(), ['rule_id' => 'rule_id']);
    }

    public function getGroups()
    {
        return $this->hasMany(Groups::className(), ['group_id' => 'group_id']); 
    }
}
