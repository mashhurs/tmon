<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Emotions;

/**
 * EmotionsSearch represents the model behind the search form about `app\models\Emotions`.
 */
class EmotionsSearch extends Emotions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['emotion_id'], 'integer'],
            [['emotion_key', 'emotion_title', 'emotion_url', 'emotion_state', 'emotion_create'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Emotions::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'emotion_id' => $this->emotion_id,
            'emotion_create' => $this->emotion_create,
        ]);

        $query->andFilterWhere(['like', 'emotion_key', $this->emotion_key])
            ->andFilterWhere(['like', 'emotion_title', $this->emotion_title])
            ->andFilterWhere(['like', 'emotion_url', $this->emotion_url])
            ->andFilterWhere(['like', 'emotion_state', $this->emotion_state]);

        return $dataProvider;
    }
}
