<?php

namespace app\components;
use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Users;
use app\models\Groups;

class Helper
{
    public static function getAgentsList()
    {
        $users = Users::find()
                        ->select(['user_id', 'user_nick'])
                        ->where(['user_role' => 'agent'])
                        ->all();
        $agents = ArrayHelper::map($users, 'user_id', 'user_nick');

        //Yii::info($agents, 'requests');

        return $agents;
    }

    public static function getGroups()
    {
    	$groups = Groups::find()
    					->select(['group_id', 'group_title'])
    					->all();
    	return ArrayHelper::map($groups, 'group_id', 'group_title');
    }
}
