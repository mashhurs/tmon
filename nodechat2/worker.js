var 	config = require('./modules/config'),
	chatserver = require('./modules/chatserver'),
	database = require('./modules/database'),
	express = require('express'),
	http = require('http');
	
var sql = new database();
var app = express();

// Routing static
app.use(express.static(config.STATIC_FOLDER));

var server = http.createServer(app);

var port_worker = process.env['port'] || config.START_PORT;	//
chatserver(server, sql);
server.listen(port_worker, function () {
	console.log('Server listening at port %d', port_worker);
});



