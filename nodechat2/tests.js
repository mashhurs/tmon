var	config = require('./modules/config'),
	database = require('./modules/database'),
	redis = require('./modules/redis'),
	storage = require('./modules/storage');

var self = this;

self.sql = new database(clearMysql);

setTimeout(function() {
	self.redis = new redis(clearNodes);
}, 1000);
/*
setTimeout(function() {
	self.storage = new storage(self.sql, self.redis);

	testStorage()
}, 2000);
*/

function clearNodes(nextFunction) {
	console.log('Clear nodes :');

	var pending = config.REDIS_CLUSTER_INFO.length;
	config.REDIS_CLUSTER_INFO.forEach(function(a_item, a_key) {
		var redis_client = self.redis.rcluster.createClient(a_item)

		// Чистим redis
		redis_client.info()(function* () {
			try {
				var chat_info = yield [
					this.flushdb(),
					this.flushall(),
				];
			} catch (ex) {
				console.log(ex);
			} finally {
//				redis_client.clientEnd();
			}
			console.log("    clean redis: ", a_item);

			if (0 === --pending && typeof nextFunction === 'function') nextFunction();
		})();


	});
}

function clearMysql(nextFunction) {
	console.log('Clear mysql :');

	self.sql.m_c.query(
		"DELETE FROM `sessions`",
		[],
		function(err, rows) {
			console.log('    clean sessions.');
			self.sql.m_c.query(
				"DELETE FROM `sessionsrelations`",
				[],
				function(err, rows) {
					console.log('    clean sessionsrelations.');
					self.sql.m_c.query(
						"DELETE FROM `messages`",
						[],
						function(err, rows) {
							console.log('    clean messages.');

							self.sql.m_c.query(
								"DELETE FROM `routings`",
								[],
								function(err, rows) {
									console.log('    clean routings.');

									self.sql.m_c.query(
										"DELETE FROM `delivmessagesusers`",
										[],
										function(err, rows) {
											console.log('    clean delivmessagesusers.');

											if (typeof nextFunction === 'function') nextFunction();
	
										}
									);
								}
							);
						}
					);
				}
			);
		}
	);


}

function testStorage(nextFunction) {
	console.log('Test storage :');

	console.log('   CREATE SESSION START:');
	self.storage.load_v5('create_session', {user_id:6}, function(info_session) {
		console.log('   CREATE SESSION END: ', info_session);

		console.log('   LOAD SESSION START:');
		self.storage.load_v5('session', {session_id:info_session.session_id}, function(info_session) {
			console.log('   LOAD SESSION END: ', info_session);

			console.log('   SAVE RELATIONS USER START:');
			self.storage.save_v5('session_relation', {session_id:info_session.session_id, user_id:3, sessionrelation_role:'manager'}, function(info_session_relation) {
				console.log('   SAVE RELATIONS USER END: ', info_session_relation);

				console.log('   SAVE RELATIONS GROUP START:');
				self.storage.save_v5('session_relation', {session_id:info_session.session_id, group_id:2, sessionrelation_role:'member'}, function(info_session_relation) {
					console.log('   SAVE RELATIONS GROUP END: ', info_session_relation);

					console.log('   LOAD RELATIONS SESSION START:');
					self.storage.load_v5('session_relations', {session_id:info_session.session_id}, function(info_session_relation) {
						console.log('   LOAD RELATIONS SESSION END: ', info_session_relation);


		console.log('   SAVE MESSAGE START:');
		self.storage.save_v5('message', {session_id:info_session.session_id, user_id:6, message_key:'ZZZ-rrr-EEE-BBBB', message_type:'text', message_data:{text:'Example text'}}, function(info_message) {
			console.log('   SAVE MESSAGE END: ', info_message);

			console.log('   SAVE DELIVERY START:');
			self.storage.save_v5('delivery', {message_id:info_message.message_id, user_id:6}, function(info_message) {
				console.log('   SAVE DELIVERY END: ', info_message);
			});

		});


					});
				});
			});
		});
	});

	setTimeout(function() {
		console.log('   LOAD USER START:');
		self.storage.load_v5('user', {user_id:6}, function(info_user) {
			console.log('   LOAD USER END: ', info_user);

			console.log('   LOAD USER RELATIONS START:');
			self.storage.load_v5('user_relations', {user_id:6}, function(info_user) {
				console.log('   LOAD USER RELATIONS END: ', info_user);

			});
		});
	}, 1000);


	setTimeout(function() {
		console.log('   LOAD GROUP START:');
		self.storage.load_v5('group', {group_id:2}, function(info_group) {
			console.log('   LOAD GROUP END: ', info_group);

			console.log('   LOAD GROUP RELATIONS START:');
			self.storage.load_v5('group_relations', {group_id:2}, function(info_group) {
				console.log('   LOAD GROUP RELATIONS END: ', info_group);

			});
		});
	}, 2000);

	setTimeout(function() {

	}, 3000);

// Создание пуша

// Загрузка всех чатов
// Загрузка хистори


//Надо амдинский чат изменить. в связи с новыми реалиями.


}

























