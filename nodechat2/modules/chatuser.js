var	config = require('./config'),
	uuid = require('node-uuid'),
	storage = require('./storage');
	redis = require('./redis');

module.exports = user;

function user(user_info, client, database){
	var self = this;

	self.create_session = false;
	self.general_info = {};
	self.user_id = user_info.user_id;
	self.user_role = user_info.user_role;

	self.client = client;		// Сокет соединение
	self.database = database;	// База
	self.redis = new redis();
	self.storage = new storage(self.database, self.redis, self.client);
}

// Объединение массивов
Array.prototype.merge = function(p_val) {
	for (var attrname in p_val) { this[attrname] = p_val[attrname]; }
}


//// НАДО ВСЕ ПУБЛИКАЦИИ С ЕДИНСТВЕННЫМ ПОЛУЧАТЕЛЕМ САМИМ СОБОЙ ПЕРЕВЕСТИ НА self.client.emit ЧТОБЫ НЕ ГОНЯТЬ ДАННЫЕ ТУДА И ОБРАТНО ПРОСТО ТАК


// --------------------- ФУНКЦИИ 5той ВЕРСИИ

user.prototype.openSessionsV5 = function(data_info) {
	var self = this;

	self.storage.load_v5('open_sessions', {user_id:self.user_id}, function(sessions_info) {
		if (sessions_info) self.publishV5({publish_type:'open_sessions_v5', list:sessions_info});
		else self.publishV5({publish_type:'open_sessions_v5', list:[]});
	});
}

user.prototype.publishV5 = function(data_info) {
	console.log('V5 Publish:', data_info.publish_type);
	var self = this;

	switch(data_info.publish_type) {
	case 'change_state_v5':
	case 'status_v5':

		var list_users = {};
		var list_groups = {};

		// Связи через чат-сессии пользователя
		self.storage.load_v5('user_relations', {user_id:data_info.user_id, sessionrelation_state:['active']}, function(user_relations) {
			user_relations.forEach(function(r_item, r_key) {
				var cur_user_relation = r_item;

				self.storage.load_v5('session_relations', {session_id:cur_user_relation.session_id, sessionrelation_state:['active']}, function(session_relations) {
					for(var i = 0; i < session_relations.length; i++) {
						var cur_session_relation = session_relations[i];

						if (cur_session_relation.user_id > 0 && typeof list_users[cur_session_relation.user_id] === 'undefined' ) {
							list_users[cur_session_relation.user_id] = true;
							self.storage.publish_user_v5(cur_session_relation.user_id, data_info);
						} else if (cur_session_relation.group_id > 0 && typeof list_groups[cur_session_relation.group_id] === 'undefined') {
							list_groups[cur_session_relation.group_id] = true;
							self.storage.load_v5('group_users', {group_id:cur_session_relation.group_id}, function(group_users) {
								for(var i = 0; i < group_users.length; i++) {
									if (group_users[i].user_id > 0 && typeof list_users[group_users[i].user_id] === 'undefined' ) {
										list_users[group_users[i].user_id] = true;
										self.storage.publish_user_v5(group_users[i].user_id, data_info);
									}
								}
							});
						}
					}
				});
			});
		});
// Сделать когда будет время, "уведомления согрупников"
//		// Связи через группы в которых участвует пользователь
//		self.storage.load_v5('user_groups', {user_id:data_info.user_id}, function(user_groups) {
//			user_groups.forEach(function(r_item, r_key) {
//				var cur_group = r_item;
//				self.storage.load_v5('group_users', {group_id:cur_group.group_id}, function(group_users) {
//					for(var i = 0; i < group_users.length; i++) {
//						if (group_users[i].user_id > 0 && typeof list_users[group_users[i].user_id] === 'undefined' ) {
//							list_users[group_users[i].user_id] = true;
//							self.storage.publish_user_v5(group_users[i].user_id, data_info);
//						}
//					}
//				});
//			});
//		});
		break;
	case 'session_action_v5':
	case 'message_delivery_v5':
	case 'relation_v5':
		var list_users = {};
		var list_groups = {};

		// Оповещаем инициатора операции
		if (data_info.user_id > 0 && typeof list_users[data_info.user_id] === 'undefined' ) {
			list_users[data_info.user_id] = true;
			self.storage.publish_user_v5(data_info.user_id, data_info);
		}

		// Оповещаем участников чат-сессии
		self.storage.load_v5('session_relations', {session_id:data_info.session_id, sessionrelation_state:['active']}, function(relations) {
			relations.forEach(function(r_item, r_key) {
				var cur_relation = r_item;

				if (cur_relation.user_id > 0 && typeof list_users[cur_relation.user_id] === 'undefined' ) {
					list_users[cur_relation.user_id] = true;
					self.storage.publish_user_v5(cur_relation.user_id, data_info);
				} else if (cur_relation.group_id > 0 && typeof list_groups[cur_relation.group_id] === 'undefined') {
					list_groups[cur_relation.group_id] = true;
					self.storage.load_v5('group_users', {group_id:cur_relation.group_id}, function(group_users) {
						for(var i = 0; i < group_users.length; i++) {
							if (group_users[i].user_id > 0 && typeof list_users[group_users[i].user_id] === 'undefined' ) {
								list_users[group_users[i].user_id] = true;
								self.storage.publish_user_v5(group_users[i].user_id, data_info);
							}
						}
					});
				}
			});
		});

		break;
	case 'user_message_v5':
		var list_users = {};
		var list_groups = {};

		self.storage.load_v5('session_relations', {session_id:data_info.session_id, sessionrelation_state:['active']}, function(relations) {
			relations.forEach(function(r_item, r_key) {
				var cur_relation = r_item;

				if (cur_relation.user_id > 0 && typeof list_users[cur_relation.user_id] === 'undefined' ) {
					self.storage.load_v5('user', {user_id:cur_relation.user_id}, function(user_info) {
						if (user_info.user_role != "customer" || data_info.message_type != "mention") {
							list_users[cur_relation.user_id] = true;
							self.storage.publish_user_v5(cur_relation.user_id, data_info);
						}
					});
				} else if (cur_relation.group_id > 0 && typeof list_groups[cur_relation.group_id] === 'undefined') {
					list_groups[cur_relation.group_id] = true;
					self.storage.load_v5('group_users', {group_id:cur_relation.group_id}, function(group_users) {
						for(var i = 0; i < group_users.length; i++) {
							if (group_users[i].user_id > 0 && typeof list_users[group_users[i].user_id] === 'undefined' ) {
								list_users[group_users[i].user_id] = true;
								self.storage.publish_user_v5(group_users[i].user_id, data_info);
							}
						}
					});
				}
			});
		});

		break;
	case 'open_sessions_v5':
	case 'user_info_v5':
	case 'session_history_v5':
	case 'all_sessions_v5':
	case 'session_relations_v5':
		// Личная рассылка
		self.storage.publish_user_v5(self.user_id, data_info);

		break;
	case 'session_history_for_manager_v5':	// отдельный хистори для менеджера, альтернативный "session_history_v5" работает относительно даты и используется кустомером
		self.storage.load_v5('session_relations', {session_id:data_info.session_id, sessionrelation_state:['active']}, function(relations) {
			relations.forEach(function(r_item, r_key) {
				var cur_relation = r_item;
				if (cur_relation.sessionrelation_role == 'manager') {
					data_info.publish_type = 'session_history_v5'
					self.storage.publish_user_v5(cur_relation.user_id, data_info);
				}
			});
		});

		break;
	case 'managed_himself_v5':
	case 'membered_himself_v5':
	case 'leave_himself_v5':
	case 'membered_other_v5':
	case 'forward_other_v5':
	case 'managed_routing_v5':
	case 'membered_routing_v5':
	case 'create_session_v5':
	case 'close_session_v5':
	case 'session_status_change_v5':
		// Рассылаем всем агентам
		self.storage.load_v5('agents', {}, function(agents_list) {
			for(var i = 0; i < agents_list.length; i++) {
				self.storage.publish_user_v5(agents_list[i].user_id, data_info);
			}
		});
		// Рассылаем всем менеджерам
		self.storage.load_v5('managers', {}, function(agents_list) {
			for(var i = 0; i < agents_list.length; i++) {
				self.storage.publish_user_v5(agents_list[i].user_id, data_info);
			}
		});

		break;
	default:
	}
}

user.prototype.loginUserV5 = function() {
	var self = this;

	// Подписываемся
	self.storage.subscribe_user_v5(self.user_id, function() {
		self.storage.change_status_v5(self.user_id, 'Online', function(status_info) {
			if (status_info) {
				status_info["publish_type"] = 'status_v5';
				self.publishV5(status_info);
			}
			console.log('V5 Login change status:', status_info);
		});
	});

}

user.prototype.logoutUserV5 = function() {
	var self = this;

	// Отписываемся
	self.storage.unsubscribe_user_v5(self.user_id, function() {
		self.storage.get_count_subscribers_v5(self.user_id, function(count_subscribers) {
			if (count_subscribers <= 0) self.storage.change_status_v5(self.user_id, 'Logout', function(status_info) {
				console.log('V5 Logout change status:', status_info);
				if (status_info) {
					status_info["publish_type"] = 'status_v5';
					self.publishV5(status_info);
				}
			});
		});
	});

/*
	self.storage.change_status_v5(self.user_id, 'Logout', function(status_info) {
		if (status_info) {
			status_info["publish_type"] = 'status_v5';
			self.publishV5(status_info);
		}
		// Отписываемся
		self.storage.unsubscribe_user_v5(self.user_id, function() {
			console.log('V5 Logout change status:', status_info);
		});
	});
*/
};

// похорошему это общая функция, хорошобы ее в storage засунуть
user.prototype.changeRelationV5 = function(data_info, call_back) {
	var self = this;

	if (data_info.user_id > 0 && data_info.session_id > 0) {
		self.storage.load_v5('user_relation', {session_id:data_info.session_id, user_id:data_info.user_id}, function(user_relation) {
			var list_role = ['member', 'manager', 'owner'];
			if (!list_role.in_array(data_info.sessionrelation_role)) data_info.sessionrelation_role = user_relation.sessionrelation_role;
			if (typeof data_info.sessionrelation_role === 'undefined') data_info.sessionrelation_role = 'member';
			if (typeof data_info.sessionrelation_role === 'undefined') return;

			var list_state = ['active', 'leave'];
			if (!list_state.in_array(data_info.sessionrelation_state)) data_info.sessionrelation_state = user_relation.sessionrelation_state;
			if (typeof data_info.sessionrelation_state === 'undefined') data_info.sessionrelation_state = 'active';
			if (typeof data_info.sessionrelation_state === 'undefined') return;

			if ( user_relation.sessionrelation_role != data_info.sessionrelation_role || user_relation.sessionrelation_state != data_info.sessionrelation_state ) {
				self.storage.save_v5('relation', {session_id:data_info.session_id, user_id:data_info.user_id, sessionrelation_role:data_info.sessionrelation_role, sessionrelation_state:data_info.sessionrelation_state}, function(relation) {
					if (typeof call_back === 'function') call_back(relation);
				});
			} else {
				if (typeof call_back === 'function') call_back(false);
			}
		});
	} else if (data_info.group_id > 0 && data_info.session_id > 0) {
		self.storage.load_v5('group_relation', {session_id:data_info.session_id, group_id:data_info.group_id}, function(group_relation) {
			var list_role = ['member', 'manager', 'owner'];
			if (!list_role.in_array(data_info.sessionrelation_role)) data_info.sessionrelation_role = group_relation.sessionrelation_role;
			if (typeof data_info.sessionrelation_role === 'undefined') data_info.sessionrelation_role = 'member';
			if (typeof data_info.sessionrelation_role === 'undefined') return;

			var list_state = ['active', 'leave'];
			if (!list_state.in_array(data_info.sessionrelation_state)) data_info.sessionrelation_state = group_relation.sessionrelation_state;
			if (typeof data_info.sessionrelation_state === 'undefined') data_info.sessionrelation_state = 'active';
			if (typeof data_info.sessionrelation_state === 'undefined') return;

			if (group_relation.sessionrelation_role != data_info.sessionrelation_role || group_relation.sessionrelation_state != data_info.sessionrelation_state) {
				self.storage.save_v5('relation', {session_id:data_info.session_id, group_id:data_info.group_id, sessionrelation_role:data_info.sessionrelation_role, sessionrelation_state:data_info.sessionrelation_state}, function(relation) {
					//relation["publish_type"] = 'relation_v5';
					//self.publishV5(relation);
					if (typeof call_back === 'function') call_back(relation);
				});
			} else {
				if (typeof call_back === 'function') call_back(false);
			}
		});
	}
}

user.prototype.userMessageV5 = function(data_info) {
	var self = this;

	if (self.user_role == 'customer') {	// Кустомер пишет сообщение
		self.storage.load_v5('open_sessions', {user_id:self.user_id}, function(session_info) {
			if (session_info) {
				self.create_session = false;
// Тут ненужен запрос на связку с чатом, владелец добавляется автоматически
				self.storage.save_v5('message', {session_id:session_info[0].session_id, user_id:self.user_id, group_id:0, message_key:data_info.message_key, message_type:data_info.message_type, message_data:data_info.message_data}, function(message_info) {
					message_info["publish_type"] = 'user_message_v5';
					self.publishV5(message_info);

					// Запускаем роутинг если чат еще неназначен и не проходил роутинг
					if (session_info[0].session_state == 'unassigned') {
						self.storage.load_v5('routing', {session_id:session_info[0].session_id}, function(routing_info) {
							if (!routing_info) {
								self.general_info['message_id'] = message_info.message_id;
								self.general_info['message_text'] = message_info.message_data.text;
								self.routingProcessV5({session_id:session_info[0].session_id, general_info:self.general_info});
							}
						});
					}
				});
			} else if (!self.create_session) {
				self.create_session = true;
				self.storage.load_v5('create_session', {user_id:self.user_id}, function(session_info) {
					session_info["publish_type"] = 'create_session_v5';
					self.publishV5(session_info);

					// Добавляем приветственное автосообщение если оно имеется
					var d = new Date();
					d.setSeconds(d.getSeconds() - 1);
					var message_create = d.toISOString();
					self.storage.load_v5('auto_message', {automessage_type:'Greeting'}, function(auto_message) {
						if (auto_message) self.storage.save_v5('message', {session_id:session_info.session_id, user_id:0, group_id:0, message_key:'', message_type:auto_message.message_type, message_data:auto_message.message_data, message_create:message_create}, function(message_info) {
							message_info["publish_type"] = 'user_message_v5';
							self.publishV5(message_info);
						});

// Тут ненужен запрос на связку с чатом, владелец добавляется автоматически
						self.storage.save_v5('message', {session_id:session_info.session_id, user_id:self.user_id, group_id:0, message_key:data_info.message_key, message_type:data_info.message_type, message_data:data_info.message_data}, function(message_info) {
							message_info["publish_type"] = 'user_message_v5';
							self.publishV5(message_info);

							// Запускаем роутинг если чат еще неназначен и не проходил роутинг
							self.general_info['message_id'] = message_info.message_id;
							self.general_info['message_text'] = message_info.message_data.text;

							self.routingProcessV5({session_id:session_info.session_id, general_info:self.general_info});
						});
					});
				});
			} else {
				// Повторяем запрос через секунду
				setTimeout(function () {
					self.create_session = false;
					self.userMessageV5(data_info);
				}, 1000)
			}
		});
	} else if (data_info.session_id > 0) {	// Не кустомер пишет сообщение в чат
		self.storage.load_v5('session', {session_id:data_info.session_id}, function(session_info) {
			if (session_info) {
// Надо добавить проверку на закрытость сессии ?
				self.changeRelationV5({session_id:session_info.session_id, user_id:self.user_id, sessionrelation_state:'active'}, function(info) {
					self.storage.save_v5('message', {session_id:session_info.session_id, user_id:self.user_id, group_id:0, message_key:data_info.message_key, message_type:data_info.message_type, message_data:data_info.message_data}, function(message_info) {
						message_info["publish_type"] = 'user_message_v5';
						self.publishV5(message_info);
//						self.pushOfflainV5(message_info);
//						self.assignManagerV3(session_info.session_id, self.user_id, {message_data:message_info.message_data});
					});
				});
			}
		});
	} else if (data_info.user_id > 0) {	//  Не кустомер пишет сообщение кустомеру (нужно будет для рассылки рекламных сообщений)
// Надо также проверить роль тправляющего (текущего) юзера, сейчас это или агент или админ
		self.storage.load_v5('user', {user_id:data_info.user_id}, function(user_info) {
			if (user_info && user_info.user_role == 'customer') {
				self.storage.load_v5('open_sessions', {user_id:data_info.user_id}, function(session_info) {
					if (session_info) {
						self.changeRelationV5({session_id:session_info[0].session_id, user_id:self.user_id, sessionrelation_state:'active'}, function(info) {
							self.storage.save_v5('message', {session_id:session_info[0].session_id, user_id:self.user_id, group_id:0, message_key:data_info.message_key, message_type:data_info.message_type, message_data:data_info.message_data}, function(message_info) {
								message_info["publish_type"] = 'user_message_v5';
								self.publishV5(message_info);
//								self.pushOfflainV5(message_info);
//								self.assignManagerV3(session_info.session_id, self.user_id, {message_data:message_info.message_data});
							});
						});
					} else {
						self.storage.load_v5('create_session', {user_id:data_info.user_id}, function(session_info) {
							self.changeRelationV5({session_id:session_info.session_id, user_id:self.user_id, sessionrelation_state:'active'}, function(info) {
								self.storage.save_v5('message', {session_id:session_info.session_id, user_id:self.user_id, group_id:0, message_key:data_info.message_key, message_type:data_info.message_type, message_data:data_info.message_data}, function(message_info) {
									message_info["publish_type"] = 'user_message_v5';
									self.publishV5(message_info);
//									self.pushOfflainV5(message_info);
//									self.assignManagerV3(session_info.session_id, self.user_id, {message_data:message_info.message_data});
								});
							});
						});
					}
				});
			}
		});
	}
}

user.prototype.sessionHistoryForManagerV5 = function(data_info) {
	var self = this;

	if (data_info.session_id > 0) {
		self.storage.load_v5('session', {session_id:data_info.session_id}, function(session_info) {
			if (session_info) self.storage.load_v5('messages', {session_id:session_info.session_id}, function(messages_list) {
				self.storage.load_v5('routing_message', {session_id:data_info.session_id}, function(routing_message) {
					if (routing_message) session_info['routing_message'] = routing_message;
					else session_info['routing_message'] = {};

					session_info["messages"] = messages_list;
					session_info['publish_type'] = 'session_history_for_manager_v5';
					self.publishV5(session_info);
				});
			});
		});
	}
}

user.prototype.sessionHistoryV5 = function(data_info) {
	var self = this;

	if (self.user_role == 'customer') {
		if (data_info.session_id > 0) {
			self.storage.load_v5('session', {session_id:data_info.session_id}, function(session_info) {
				if (session_info && session_info.user_id == self.user_id) self.storage.load_v5('session_history', {user_id:self.user_id, session_create:session_info.session_create}, function(session_history) {
					session_history['publish_type'] = 'session_history_v5';
					self.publishV5(session_history);
				});
			});
		} else {
			self.storage.load_v5('session_history', {user_id:self.user_id}, function(session_history) {

// Надо также воткнуть OffHours авто-сообщение если время не рабочее
// Его же вставить при создании чат-сессии

				self.storage.load_v5('auto_message', {automessage_type:'Greeting'}, function(auto_message) {
					if (auto_message && typeof session_history.session_state === 'undefined' || session_history.session_status == 'close') {
						if (typeof session_history.messages === 'undefined') session_history.messages = [];

						session_history.messages.push(auto_message);
					}
					session_history['publish_type'] = 'session_history_v5';
					self.publishV5(session_history);
				});
			});
		}
	} else {
		if (data_info.session_id > 0) {
			self.storage.load_v5('session', {session_id:data_info.session_id}, function(session_info) {
				if (session_info) self.storage.load_v5('messages', {session_id:session_info.session_id}, function(messages_list) {
					session_info["messages"] = messages_list;
					session_info['publish_type'] = 'session_history_v5';
					self.publishV5(session_info);
				});
			});
		}
	}
};

user.prototype.messageDeliveryV5 = function(data_info) {
	var self = this;

// Тут надо проверить наличие этого сообщения, наличие текущего юзера в чат-сесии этого сообщения
// но можно и забить, не особо кретично

	data_info.user_id = self.user_id;
	self.storage.save_v5('delivery', data_info, function(delivery_info) {
		delivery_info['publish_type'] = 'message_delivery_v5';
		self.publishV5(delivery_info);
	});
}

user.prototype.sessionActionV5 = function(data_info) {
	var self = this;

// Надо проверить нахождение этого пользователя в указанной чат-чессии и только потом публиковать
	if (self.user_role == 'customer') {
		self.storage.load_v5('open_sessions', {user_id:self.user_id}, function(sessions_info) {
			if (sessions_info) {
				var session_action = {
					session_id:sessions_info[0].session_id,
					user_id:self.user_id,
					action:data_info.action,
					action_create:new Date().toISOString(),
					publish_type:'session_action_v5',
				};
                
				self.publishV5(session_action);
			}
		});
	} else {
		var session_action = {
			session_id:data_info.session_id,
			user_id:self.user_id,
			action:data_info.action,
			action_create:new Date().toISOString(),
			publish_type:'session_action_v5',
		};

		self.publishV5(session_action);
	}

}

user.prototype.allSessionsV5 = function(data_info) {
	var self = this;

	if (self.user_role != 'customer') {
		self.storage.load_v5('all_sessions', {session_id:data_info.session_id}, function(sessions_list) {
			self.publishV5({publish_type:'all_sessions_v5', list:sessions_list});
		})
	}
}

user.prototype.changeStateV5 = function(data_info) {
	var self = this;

	self.storage.load_v5('change_state', {user_state:data_info.user_state,user_id:self.user_id}, function(state_result) {
		if (state_result) {
			state_result['publish_type'] = 'change_state_v5';
			self.publishV5(state_result);
		}
	})

}

user.prototype.userInfoV5 = function(data_info) {
	var self = this;

	if (self.user_role != 'customer') {
		self.storage.load_v5('user', {user_id:data_info.user_id}, function(user_info) {
			user_info['publish_type'] = 'user_info_v5';
			self.publishV5(user_info);
		});
	}
};

user.prototype.sessionRelationsV5 = function(data_info) {
	var self = this;

	if (self.user_role != 'customer') {
		self.storage.load_v5('session_relations', {session_id:data_info.session_id}, function(relations_list) {
			self.publishV5({publish_type:'session_relations_v5', list:relations_list});
		});
	}
};

user.prototype.managedHimselfV5 = function(data_info) {
	var self = this;

	if (self.user_role != 'customer') self.storage.load_v5('session', {session_id:data_info.session_id}, function(session_info) {
		// Назначить на себя можно только чаты в состоянии 'unassigned'
		if (session_info && session_info.session_state == 'unassigned') {
			self.changeRelationV5({user_id:self.user_id, session_id:data_info.session_id, sessionrelation_role:'manager', sessionrelation_state:'active'}, function(relation_info) {
				// Перевести сессию в разряд назначенных
				if (relation_info) self.storage.save_v5('assigned_session', {session_id:data_info.session_id}, function(assigned_info) {
					relation_info["publish_type"] = 'managed_himself_v5';
					self.publishV5(relation_info);
				});
			});
		}
	});
};

user.prototype.subscribeMemberV5 = function(data_info) {
	var self = this;

	if (self.user_role != 'customer') {
		self.storage.load_v5('user_relation', {user_id:self.user_id, session_id:data_info.session_id}, function(user_relation) {
			if (!user_relation || user_relation.sessionrelation_role != 'manager') {
				self.changeRelationV5({user_id:self.user_id, session_id:data_info.session_id, sessionrelation_role:'member', sessionrelation_state:'active'}, function(relation_info) {
					if (relation_info) {
						relation_info["publish_type"] = 'subscribe_member_v5';
						self.publishV5(relation_info);
					}
				});
			}
		});
	}

};

user.prototype.unsubscribeMemberV5 = function(data_info) {
	var self = this;

	if (self.user_role != 'customer') {
		self.storage.load_v5('user_relation', {user_id:self.user_id, session_id:data_info.session_id}, function(user_relation) {
			if (user_relation && sessionrelation_role == 'member') {
				self.changeRelationV5({user_id:self.user_id, session_id:data_info.session_id, sessionrelation_state:'leave'}, function(relation_info) {
					if (relation_info) {
						relation_info["publish_type"] = 'unsubscribe_member_v5';
						self.publishV5(relation_info);
					}
				});
			}
		});
	}
};

//user.prototype.memberedHimselfV5 = function(data_info) {
//	var self = this;
//
//	if (self.user_role != 'customer') self.changeRelationV5({user_id:self.user_id, session_id:data_info.session_id, sessionrelation_role:'member', sessionrelation_state:'active'}, function(relation_info) {
//		if (relation_info) {
//			relation_info["publish_type"] = 'membered_himself_v5';
//			self.publishV5(relation_info);
//		}
//	});
//};

user.prototype.leaveHimselfV5 = function(data_info) {
	var self = this;

	if (self.user_role != 'customer') self.changeRelationV5({user_id:self.user_id, session_id:data_info.session_id, sessionrelation_state:'leave'}, function(relation_info) {
		if (relation_info) {
			relation_info["publish_type"] = 'leave_himself_v5';
			self.publishV5(relation_info);
		}
	});
};

//user.prototype.memberedOtherV5 = function(data_info) {
//	var self = this;
//
//	if (self.user_role != 'customer') {
//// Надо еще добавляемого мембера проверить является ли он агентом, чтобы юзеров не добавляли
//// но это не критично
//		self.changeRelationV5({user_id:data_info.user_id, session_id:data_info.session_id, sessionrelation_role:'member', sessionrelation_state:'active'}, function(relation_info) {
//			if (relation_info) {
//				relation_info["publish_type"] = 'membered_other_v5';
//				self.publishV5(relation_info);
//			}
//		});
//	}
//};

user.prototype.forwardOtherV5 = function(data_info) {
	var self = this;

	if (self.user_role != 'customer') {
// По идее надо проверять являюсь ли я сам менеджером, а также является ли назначаемый агентом
		self.storage.load_v5('session_relations', {session_id:data_info.session_id, sessionrelation_state:['active']}, function(relations_list) {
			for(var i = 0; i < relations_list.length; i++) {

				// Проверяем чтобы я был менеджером чата который передаю
				if (relations_list[i].user_id == self.user_id && relations_list[i].sessionrelation_role == 'manager' && relations_list[i].sessionrelation_state == 'active') {
					self.changeRelationV5({user_id:data_info.user_id, session_id:data_info.session_id, sessionrelation_role:'manager', sessionrelation_state:'active'}, function(relation_info_t) {
						if (relation_info_t) {
							self.changeRelationV5({user_id:self.user_id, session_id:data_info.session_id, sessionrelation_role:'member', sessionrelation_state:'active'}, function(relation_info_f) {
								if (relation_info_f) {
									relation_info_t['publish_type'] = 'forward_other_v5'
									self.publishV5(relation_info_t);
									//{publish_type:'forward_other_v5', session_id:data_info.session_id, user_id:self.user_id, to:relation_info_t, from:relation_info_f}
								}
							});
						}
					});
					break;
				}
			}
		});
// Форвардить можно только свои чаты в которых агент менеджер.
	}
};


user.prototype.routingInfoV5 = function(data_info) {
	var self = this;

	self.general_info.merge(data_info);
}


// Внутренняя функция подбора роутинга
user.prototype.routingProcessV5 = function(data_info) {
	var self = this;
	/*
		data_info = {
			* session_id:
			* general_info:{
				device_type:
				message_id:
				message_text:
				...
			}
		}
	*/
	var general_info = data_info.general_info
	var select_rules_info;

	console.log('V5 Inicial routing for session_id:', data_info.session_id);
	console.log('Data for routing:', general_info);

	self.storage.load_v5('rules', {}, function(rules_list) {                                                                               	
		for(var i = 0; i < rules_list.length; i++) {
			var count = 0;
			cur_rules_info = JSON.parse(rules_list[i].rule_info);
			for(var w = 0; w < cur_rules_info.conditions.length; w++) {
				if (cur_rules_info.conditions[w].strict == 'complete') {
					if (typeof general_info[cur_rules_info.conditions[w].key] !== 'undefined' && general_info[cur_rules_info.conditions[w].key] == cur_rules_info.conditions[w].value) count++;
				} else if (cur_rules_info.conditions[w].strict == 'overlap') {
					if (typeof general_info[cur_rules_info.conditions[w].key] !== 'undefined' && general_info[cur_rules_info.conditions[w].key].indexOf(cur_rules_info.conditions[w].value) >= 0) count++;
				}
			}
			if (cur_rules_info.type == "OR" && count > 0) {
				select_rules_info = rules_list[i];
				break;
			} else if (cur_rules_info.type == "AND" && count == cur_rules_info.conditions.length) {
				select_rules_info = rules_list[i];
				break;
			}
		}

		if (typeof select_rules_info !== 'undefined') {
			console.log('Rules found:', select_rules_info);
			self.storage.save_v5('routing', {session_id:data_info.session_id, rule_id:select_rules_info.rule_id, message_id:general_info.message_id}, function() {
				if (select_rules_info.user_id > 0) {
					// Если в роутинге указан Агент - зразуже подгружаем его данные проверям и назначаем если можем
					self.storage.load_v5('user', {user_id:select_rules_info.user_id}, function(user_info) {
						self.storage.load_v5('user_relations', {user_id:user_info.user_id, sessionrelation_role:['manager'], sessionrelation_state:['active']}, function(user_relations) {
							// Берем только пользователей онлайн, и если есть места у него
							if (user_info.user_status == 'Online' && user_info.user_state == 'Online' && user_relations.length < user_info.user_capacity) {
								self.changeRelationV5({user_id:user_info.user_id, session_id:data_info.session_id, sessionrelation_role:'manager', sessionrelation_state:'active'}, function(relation_info) {
									if (relation_info) self.storage.save_v5('allocated_session', {session_id:data_info.session_id}, function(assigned_info) {
										relation_info["publish_type"] = 'managed_routing_v5';
										self.publishV5(relation_info);
										self.sessionHistoryForManagerV5({session_id:data_info.session_id});
									});
								});
        						}
						});
					});
				} else if (select_rules_info.group_id > 0) {
					// Если в роутинге назначена группа, пытаемся назначить ее или ее backup
					self.storage.load_v5('group', {group_id:select_rules_info.group_id}, function(group_info) {
						if (group_info) self.storage.load_v5('group_relations', {group_id:select_rules_info.group_id, sessionrelation_state:['active']}, function(group_relations) {
							// Проверяем вместимость основной группы
							if (group_relations.length < group_info.group_capacity) {
								self.changeRelationV5({group_id:select_rules_info.group_id, session_id:data_info.session_id, sessionrelation_role:'member', sessionrelation_state:'active'}, function(relation_info) {
									if (relation_info) self.storage.save_v5('assigned_session', {session_id:data_info.session_id}, function(assigned_info) {
										relation_info["publish_type"] = 'membered_routing_v5';
										self.publishV5(relation_info);
										self.allocationSessionV5();
									});
								});
							} else if (group_info.group_backup > 0) {
								console.log('Main target routing is full, check backup:', group_info.group_backup);

								// Проверяем вместимость backup группы
								self.storage.load_v5('group', {group_id:group_info.group_backup}, function(group_info) {
									if (group_info) self.storage.load_v5('group_relations', {group_id:group_info.group_backup, sessionrelation_state:['active']}, function(group_relations) {

										if (group_relations.length < group_info.group_capacity) {
											self.changeRelationV5({group_id:select_rules_info.group_id, session_id:data_info.session_id, sessionrelation_role:'member', sessionrelation_state:'active'}, function(relation_info) {
												if (relation_info) self.storage.save_v5('assigned_session', {session_id:data_info.session_id}, function(assigned_info) {
													relation_info["publish_type"] = 'membered_routing_v5';
													self.publishV5(relation_info);
													self.allocationSessionV5();
												});
											});
										}
									});
								});
							}
						});
					});
				} else return;
			});
		}
	});
};

// Внутренняя функция подбора роутинга
user.prototype.allocationProcessV5 = function(data_info) {
	var self = this;
	/*
		data_info = {
			* session_id:
		}
	*/

	var list_users = [];

	console.log('V5 Start allocation for session_id:', data_info.session_id);
	self.storage.load_v5('session_relations', {session_id:data_info.session_id, sessionrelation_state:['active']}, function(session_relations) {                                                                               	
		// Надо пробежаться по всему списку и найти там Группу и Ненайти Менеджера
		var group_id = 0;
		var manager_id = 0;
		for(var i = 0; i < session_relations.length; i++) {
			if (session_relations[i].sessionrelation_role == 'manager') manager_id = session_relations[i].user_id;
			if (session_relations[i].group_id > 0) group_id = session_relations[i].group_id;
		}

		if (session_relations.length > 0 && manager_id == 0 && group_id > 0) {
			self.storage.load_v5('group_users', {group_id:group_id}, function(group_users) {
				console.log('Found agent for allocation:', group_users.length);

				if (group_users.length > 0) {
					var pending = group_users.length;
					group_users.forEach(function(a_item, a_key) {
						self.storage.load_v5('user', {user_id:group_users[a_key].user_id}, function(user_info) {
							self.storage.load_v5('user_relations', {user_id:user_info.user_id, sessionrelation_role:['manager'], sessionrelation_state:['active']}, function(user_relations) {
								// Берем только пользователей онлайн
								if (user_info.user_status == 'Online' && user_info.user_state == 'Online') {
									var cur_user = {
										user:user_info,
										relations:user_relations,					
									};
									// Вытаскиваем самую старую дату из релейшенов
									for(var w = 0; w < user_relations.length; w++) {
										var date = new Date(user_relations[w].sessionrelation_create);
										if (typeof cur_user.min_date === 'undefined' || date.getTime < cur_user.min_date) cur_user.min_date = date.getTime;
									};
									list_users.push(cur_user);
        							}


								if (0 === --pending) {
									console.log('Final candidats allocation:', JSON.stringify(list_users));
									if (list_users.length <= 0) return;

									var finalist;
									for(var f = 0; f < list_users.length; f++) {
										var cur_candidat = list_users[f];
										// Проверяем есть ли свободное место у текущего кандидата
										if (cur_candidat.relations.length < cur_candidat.user.user_capacity) {
											// Если еще нет финалиста сразу назначаем текущего
											if (typeof finalist === 'undefined') finalist = cur_candidat;
											else {
												// Если финалист уже есть сравниваем его с текущим
												if (cur_candidat.relations.length < finalist.relations.length) finalist = cur_candidat;
												else if (cur_candidat.relations.length == finalist.relations.length && cur_candidat.min_date < finalist.min_date) finalist = cur_candidat;
											}
										}
									}
/*
									var finalist = list_users.reduce(function(result, element) {
										if (typeof result === 'undefined') return element;
										else if (element.relations.length < element.user.user_capacity) {
 											if (element.relations.length < result.relations.length 
												|| (element.relations.length == result.relations.length 
												&& (element.min_date < result.min_date 
												|| typeof result.min_date === 'undefined'))) {

												return element;
											}
										}
									});
*/
									console.log('Winner allocation:', JSON.stringify(finalist));

									// Покидаем группу
									//if (typeof finalist !== 'undefined') self.changeRelationV5({group_id:group_id, session_id:data_info.session_id, sessionrelation_state:'leave'}, function() {
										// Назначаем агента
										self.changeRelationV5({user_id:finalist.user.user_id, session_id:data_info.session_id, sessionrelation_role:'manager', sessionrelation_state:'active'}, function(relation_info) {
											if (relation_info) self.storage.save_v5('allocated_session', {session_id:data_info.session_id}, function(assigned_info) {
												relation_info["publish_type"] = 'managed_routing_v5';
												self.publishV5(relation_info);
												self.sessionHistoryForManagerV5({session_id:data_info.session_id});
											});
										});
									//});
								}
							});
						});
					});
				}
			});
		}
	});
};


user.prototype.routingSessionV5 = function(data_info) {
	var self = this;

	// Загружаем данные пользователя в общий массиив
	var general_info = {
		device_type: (typeof data_info.device_type !== 'undefined') ? data_info.device_type : '',
		message_id: data_info.message_id,
		message_text: (typeof data_info.message_text !== 'undefined') ? data_info.message_text : '',
	};

	self.storage.load_v5('message', {message_id:data_info.message_id}, function(message_info) {                                                                               	
		if (message_info && typeof message_info.message_data.text !== 'undefined') {
			general_info['message_text'] = message_info.message_data.text;
			self.routingProcessV5({session_id:data_info.session_id, general_info:general_info});
		}
	});
}

user.prototype.allocationSessionV5 = function(data_info) {
	var self = this;

//	self.allocationProcessV5({session_id:data_info.session_id});
//	// Загрузить все сессии со статусом "assigned" в порядке очередности и идти уже по этому списку
//	self.storage.load_v5('sessions', {session_state:['assigned']}, function(sessions_list) {
//		sessions_list.forEach(function(r_item, r_key) {
//			self.allocationProcessV5({session_id:r_item.session_id});
//		});
//	});

	// Загрузить все сессии со статусом "assigned" в порядке очередности и идти уже по этому списку
	self.storage.load_v5('sessions', {session_state:['assigned']}, function(sessions_list) {
		sessions_list.forEach(function(r_item, r_key) {
			setTimeout(function () {
				self.allocationProcessV5({session_id:r_item.session_id});
			}, 100 * (r_key + 1));
		});
	});


}

//user.prototype.closeSessionV5 = function(data_info) {
//	var self = this;
//
//	if (self.user_role != 'customer') {
//		// Закрывающий сессию должен быть ее менеджером
//		self.storage.load_v5('user_relation', {user_id:self.user_id, session_id:data_info.session_id}, function(user_relation) {                                                                               	
//			if (user_relation.sessionrelation_role == 'manager' && user_relation.sessionrelation_state == 'active') self.storage.save_v5('close_session', {session_id:data_info.session_id}, function(session_info) {
//				if (session_info) {
//					session_info["publish_type"] = 'close_session_v5';
//					self.publishV5(session_info);
//				}
//			});
//		});
//	}
//}

user.prototype.managedOtherV5 = function(data_info) {
	var self = this;

	if (self.user_role == 'administrator') self.storage.load_v5('session', {session_id:data_info.session_id}, function(session_info) {
		if (session_info) self.storage.load_v5('session_relations', {session_id:data_info.session_id}, function(session_relations) {
			session_relations.forEach(function(a_item, a_key) {
				if (a_item.user_id != data_info.user_id && a_item.sessionrelation_role == 'manager' && a_item.sessionrelation_state == 'active') {
					self.changeRelationV5({user_id:a_item.user_id, session_id:a_item.session_id, sessionrelation_role:'member'}, function(relation_info) {
console.log('--->', 'Not notify Change Relation');
					});
				}
			});
			self.changeRelationV5({user_id:data_info.user_id, session_id:data_info.session_id, sessionrelation_role:'manager'}, function(relation_info) {
				if (relation_info) self.storage.save_v5('assigned_session', {session_id:data_info.session_id}, function(assigned_info) {
console.log('--->', 'Not notify Change Relation');
				});
			});
		});
	});

}

user.prototype.mentionMessageV5 = function(data_info) {
	var self = this;

	var final_list = [];
	self.storage.load_v5('session_relations', {session_id:data_info.session_id, sessionrelation_state:['active']}, function(session_relations) {
		// Убираем из списка тех кто уже есть в чат-сесиии чтобы не перекрыть менеджеров
		for(var u = 0; u < data_info.list_users.length; u++) {
			var allowed = true;
			for(var r = 0; r < session_relations.length; r++) {
				if (data_info.list_users[u] == session_relations[r]['user_id']) allowed = false;
			}
			if (allowed) final_list.push(data_info.list_users[u]);
		}

		if ( final_list.length <= 0) {
console.log('--->', 'Not notify Mention Complite');
			delete data_info['list_users'];
			self.userMessageV5(data_info)
		} else {
			var pending = final_list.length;

			final_list.forEach(function(a_item, a_key) {
				self.changeRelationV5({user_id:a_item, session_id:data_info.session_id, sessionrelation_role:'member', sessionrelation_state:'active'}, function(relation_info) {
					if (0 === --pending) {
console.log('--->', 'Not notify Mention Complite');
						delete data_info['list_users'];
						self.userMessageV5(data_info)
					}
				});
			});
		}
	});
}

user.prototype.sessionStatusChangeV5 = function(data_info) {
	var self = this;

	if (self.user_role == 'agent') {
// Проверяем являюсь ли я менеджером этой чат-сессии
		self.storage.load_v5('user_relation', {session_id:data_info.session_id, user_id:self.user_id}, function(user_relation) {
			if (user_relation && user_relation.sessionrelation_role == 'manager') {
				var list_status = ['active', 'pending', 'close'];
				if (list_status.in_array(data_info.session_status)) {
					self.storage.save_v5('status_session', {session_id:data_info.session_id, session_status:data_info.session_status}, function (status_session) {
						if (status_session) {
							status_session["publish_type"] = 'session_status_change_v5';
							self.publishV5(status_session);
						}
					});
				}
			}
		});
	} else if (self.user_role == 'administrator') {
		var list_status = ['active', 'pending', 'close'];
		if (list_status.in_array(data_info.session_status)) {
			self.storage.save_v5('status_session', {session_id:data_info.session_id, session_status:data_info.session_status}, function (status_session) {
				if (status_session) {
					status_session["publish_type"] = 'session_status_change_v5';
					self.publishV5(status_session);
				}
			});
		}
	}
}
// --------------------- ФУНКЦИИ 5той ВЕРСИИ
