var 	config = require('./config'),
	socketio = require('socket.io'),
	chatuser = require('./chatuser');


module.exports = init;

function init(server, database) {


try {

	var io = socketio.listen(server);
	io.set('heartbeat interval', config.HEARTBEAT_INTERVAL);
	io.set('heartbeat timeout', config.HEARTBEAT_TIMEOUT);

	var users = [];	// Список активных пользователей
	io.of('/chat').on('connection', function(client) {
		console.log('New connect...');
		var user_id = client.request._query['user_id'];
		var user_token = client.request._query['user_token'];

		database.m_c.query(
			"SELECT `user_id`, `user_login`, `user_nick`, `user_role`, `user_state`, `user_capacity` FROM `users` WHERE `user_id` = ?",
			[user_id], 
			function(err, user_rows) {
				if (!err) {
					if (user_rows.length > 0 && (user_token == -1 || user_rows[0].auth_key == user_token)) {

						console.log('User connect: ' + user_id);
                                
						// Заносим авторизованного пользователя в список подключенных
						if (typeof users[user_id] === 'undefined') users[user_id] = [];
						var user = new chatuser(user_rows[0], client, database);
//						users[user_id].push(user);
                                
						// Уведомляем базу о пользователе онлайн
						user.loginUserV5();                                
                                
						client.on('disconnect', function () {
							console.log('V5 User disconnect.');
							// Пройтись по чатам в которых есть 
							//	                database.setUserState(user_id, "offline");
//							if (typeof users[user_id] !== 'undefined') users[user_id].forEach(function(value, key, origin) { 
								user.logoutUserV5();
//							});
//							delete users[user_id];
						});

// --------------- ФУНКЦИИ 5той ВЕРСИИ

						// Дополнительные сведения о клиенте (для роутинга)
						client.on('routing_info_v5', function (data) {
							console.log('routing_info_v5:', data);

							user.routingInfoV5(data);
						});

						// Отправляем сообщение
						client.on('user_message_v5', function (data) {
							console.log('user_message_v5:', data);

							user.userMessageV5(data);
						});

						// Отправляем сообщение с упоминанием агентов
						client.on('mention_message_v5', function (data) {
							console.log('mention_message_v5:', data);

							user.mentionMessageV5(data);
						});

						// История сессий с сообщениями
						client.on('session_history_v5', function (data) {
							console.log('session_history_v5:', data);

							user.sessionHistoryV5(data);
						});

						// Подтверждение получения сообщения
						client.on('message_delivery_v5', function (data) {
							console.log('message_delivery_v5:', data);

							user.messageDeliveryV5(data);
						});

						// Публикуем свою активность в чате
						client.on('session_action_v5', function (data) {
							console.log('session_action_v5:', data);

							user.sessionActionV5(data);
						});

						// Публикуем свою активность в чате
						client.on('all_sessions_v5', function (data) {
							console.log('all_sessions_v5:', data);

							user.allSessionsV5(data);
						});

						// Смена статуса
						client.on('change_state_v5', function (data) {
							console.log('change_state_v5:', data);

							user.changeStateV5(data);
						});

						// Запрос информации о пользователе
						client.on('user_info_v5', function (data) {
							console.log('user_info_v5:', data);

							user.userInfoV5(data);
						});

						// Запрос информации об участниках чат-сессии
						client.on('session_relations_v5', function (data) {
							console.log('session_relations_v5:', data);

							user.sessionRelationsV5(data);
						});



						// Назначить себя менеджером чат-сессии
						client.on('managed_himself_v5', function (data) {
							console.log('managed_himself_v5:', data);

							user.managedHimselfV5(data);
						});

						// Подписываемся на чат-сессию
						client.on('subscribe_member_v5', function (data) {
							console.log('subscribe_member_v5:', data);

							user.subscribeMemberV5(data);
						});

						// Отписываемся от чат-сессии
						client.on('unsubscribe_member_v5', function (data) {
							console.log('unsubscribe_member_v5:', data);

							user.unsubscribeMemberV5(data);
						});

//						// Принять участвие в чат-сессии
//						client.on('membered_himself_v5', function (data) {
//							console.log('membered_himself_v5:', data);
//
//							user.memberedHimselfV5(data);
//						});

						// Покинуть чат-сессию
						client.on('leave_himself_v5', function (data) {
							console.log('leave_himself_v5:', data);

							user.leaveHimselfV5(data);
						});

//						// Пригласить в чат-сессию другого агента
//						client.on('membered_other_v5', function (data) {
//							console.log('membered_other_v5:', data);
//
//							user.memberedOtherV5(data);
//						});

						// Передать чат-сессию другому агенту
						client.on('forward_other_v5', function (data) {
							console.log('forward_other_v5:', data);

							user.forwardOtherV5(data);
						});


						// Ручной роутин чат-сессии
						client.on('routing_session_v5', function (data) {
							console.log('routing_session_v5:', data);

							user.routingSessionV5(data);
						});

						// Ручной алокейшен чат-сессии находящейся в группе
						client.on('allocation_session_v5', function (data) {
							console.log('allocation_session_v5:', data);

							user.allocationSessionV5(data);
						});


//						// Закрытие сессии агентом
//						client.on('close_session_v5', function (data) {
//							console.log('close_session_v5:', data);
//
//							user.closeSessionV5(data);
//						});

						// Назначить указанного агента менеджером
						client.on('managed_other_v5', function (data) {
							console.log('managed_other_v5:', data);

							user.managedOtherV5(data);
						});

						// Меняем статус чат-сессиии
						client.on('session_status_change_v5', function (data) {
							console.log('session_status_change_v5:', data);

							user.sessionStatusChangeV5(data);
						});










						// Сообщаем юзеру что он авторизован
						client.emit('login_v5', user_rows[0]);

// --------------- ФУНКЦИИ 5той ВЕРСИИ


						return;
					}					
				}


				console.log('Broken login.');
				client.disconnect();
				return;
			}
		);

//		client.on('disconnect', function () {
//			console.log('Disconnect... user_id: ' + user_id);
//		});

		client.on('error', function (err) {
			console.log('Error... user_id: ' + user_id);
		});
	});


} catch (ex) {
//	sql.m_c.query(
//		"INSERT DELAYED INTO `logs` (`user_id`, `log_type`, `log_data`) VALUES(?, ?, ?)",
//		[0, 'unknown_except', JSON.stringify(ex)]
//	);
	console.log("unknown_except: ", JSON.stringify(ex));
}

}