var 	config = require('./config'),
	mysql = require('mysql');

module.exports = Database;

function Database() {
	this.connectDatabase();
}

function Database(onconnect) {
	this.onconnect = onconnect;
	this.connectDatabase();
}

Database.prototype.connectDatabase = function() {
	var self = this;

	// Mysql
	self.m_c = mysql.createConnection(config.MYSQL_INFO);
	self.m_c.connect(function(err){
		if(!err) {
			console.log("Database is connected.");  
			if (typeof self.onconnect === 'function') self.onconnect();
		} else console.log("Error connecting database !");  
	});

	self.m_c.on('error', function(err) {
		console.log('db error', err);
		if(err.code === 'PROTOCOL_CONNECTION_LOST') {
			self.m_c = mysql.createConnection(config.MYSQL_INFO);
		} else {
			throw err;
		}
	});

	self.escape = mysql.escape;
}


