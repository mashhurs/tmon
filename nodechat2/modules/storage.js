module.exports = Storage;

function Storage(mysql, redit, socket) {
	var self = this;

	self._container = {users:{}, chats:{}, relation:{}};

	self._mysql = mysql;
	self._redis = redit;
	self._socket = socket;
}

// Объединение массивов
Array.prototype.merge = function(p_val) {
	for (var attrname in p_val) { this[attrname] = p_val[attrname]; }
}

// Проверка пристусвия элемента в массиве
Array.prototype.in_array = function(p_val) {
	for(var i = 0, l = this.length; i < l; i++) {
		if(this[i] == p_val) {
			return true;
		}
	}
	return false;
}

// --------------- ФУНКЦИИ 5той ВЕРСИИ

// Публикация через редис (решает проблемы мультиклиентов)
Storage.prototype.publish_user_v5 = function(user_id, data_info, call_back) {
 	var self = this;
	self._redis.rc_out.info()(function* () {
		yield this.publish("user:" + user_id, JSON.stringify(data_info));
		//console.log("V5 Publish in redis to user:", user_id, data_info);
		if (typeof call_back === 'function') call_back();
	})();
}

// Перечитываем из мускула только перед вставкой данных
Storage.prototype.save_v5 = function(data_type, data_info, call_back) {
	var self = this;

	switch(data_type) {
	case "relation":
		/*
			data_info = {
				* session_id
				* user_id | group_id
				sessionrelation_role = 'member'
				sessionrelation_state = 'active'
			}
		*/
		var sessionrelation_create = new Date().toISOString();
		var sessionrelation_update = sessionrelation_create;

		var list_role = ['member', 'manager', 'owner'];
		if (!list_role.in_array(data_info.sessionrelation_role)) data_info.sessionrelation_role = 'member';

		var list_state = ['active', 'leave'];
		if (!list_state.in_array(data_info.sessionrelation_state)) data_info.sessionrelation_state = 'active';

		if (typeof data_info.session_id === 'undefined') return;
		if (typeof data_info.user_id === 'undefined') data_info.user_id = 0;
		if (typeof data_info.group_id === 'undefined') data_info.group_id = 0;
		if (data_info.user_id == 0 && data_info.group_id == 0) return;

		self._mysql.m_c.query(
			"INSERT INTO `sessionsrelations` (`session_id`, `user_id`, `group_id`, `sessionrelation_role`, `sessionrelation_state`, `sessionrelation_update`, `sessionrelation_create`) VALUES (?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `sessionrelation_id`=LAST_INSERT_ID(`sessionrelation_id`), `sessionrelation_role` = VALUES(`sessionrelation_role`), `sessionrelation_state` = VALUES(`sessionrelation_state`), `sessionrelation_update` = VALUES(`sessionrelation_update`), `sessionrelation_create` = VALUES(`sessionrelation_create`)",
			[data_info.session_id, data_info.user_id, data_info.group_id, data_info.sessionrelation_role, data_info.sessionrelation_state, sessionrelation_update, sessionrelation_create],
			function(err, rows) {
				if (!err) {
					self._redis.rc_out.info()(function* () {
						if (data_info.user_id > 0) {
							yield [
								this.sadd("session:" + data_info.session_id + ":relations", "U_" + data_info.user_id),
								this.sadd("user:" + data_info.user_id + ":relations", data_info.session_id),
								this.hset("relations:" + data_info.session_id + "_U_" + data_info.user_id, "sessionrelation_role", data_info.sessionrelation_role),
								this.hset("relations:" + data_info.session_id + "_U_" + data_info.user_id, "sessionrelation_state", data_info.sessionrelation_state),
								this.hset("relations:" + data_info.session_id + "_U_" + data_info.user_id, "sessionrelation_update", sessionrelation_update),
								this.hset("relations:" + data_info.session_id + "_U_" + data_info.user_id, "sessionrelation_create", sessionrelation_create),
							];
						} else if (data_info.group_id > 0) {
							yield [
								this.sadd("session:" + data_info.session_id + ":relations", "G_" + data_info.group_id),
								this.sadd("group:" + data_info.group_id + ":relations", data_info.session_id),
								this.hset("relations:" + data_info.session_id + "_G_" + data_info.group_id, "sessionrelation_role", data_info.sessionrelation_role),
								this.hset("relations:" + data_info.session_id + "_G_" + data_info.group_id, "sessionrelation_state", data_info.sessionrelation_state),
								this.hset("relations:" + data_info.session_id + "_G_" + data_info.group_id, "sessionrelation_update", sessionrelation_update),
								this.hset("relations:" + data_info.session_id + "_G_" + data_info.group_id, "sessionrelation_create", sessionrelation_create),
							];
						}

						console.log("V5 Save sessionrelation: ");
						if (typeof call_back === 'function') call_back({session_id:data_info.session_id, user_id:data_info.user_id, group_id:data_info.group_id, sessionrelation_role:data_info.sessionrelation_role, sessionrelation_state:data_info.sessionrelation_state, sessionrelation_update:sessionrelation_update, sessionrelation_create:sessionrelation_create});
					})();
				} else console.log("SQL ERROR save sessionrelation: ", err);
			}
		);

		break;
	case "assigned_session":
		/*
			data_info = {
				* session_id
			}
		*/
		var session_update = new Date().toISOString();

		self.load_v5('session', {session_id:data_info.session_id}, function(session_info) {
			if (session_info) {
				self._mysql.m_c.query(
					"UPDATE `sessions` SET `session_state` = 'assigned', `session_update` = ? WHERE `session_id` = ? AND `session_state` = 'unassigned' AND `session_status` != 'close'",
					[session_update, data_info.session_id],
					function(err, rows) {
						if (!err) {
							if (rows.changedRows > 0) {
								session_info['session_state'] = 'assigned';
								session_info['session_update'] = 'session_update';

								// Кешируем данные чата в редис
								self._redis.rc_out.info()(function* () {
									var chat_info = yield [
										this.hset("session:" + session_info['session_id'], "session_id", session_info['session_id']),
										this.hset("session:" + session_info['session_id'], "session_state", session_info['session_state']),
										this.hset("session:" + session_info['session_id'], "session_status", session_info['session_status']),
										this.hset("session:" + session_info['session_id'], "user_id", session_info['user_id']),
										this.hset("session:" + session_info['session_id'], "session_update", session_info['session_update']),
										this.hset("session:" + session_info['session_id'], "session_create", session_info['session_create']),
									];

									if (typeof call_back === 'function') call_back(session_info);
								})();
							} else {
								if (typeof call_back === 'function') call_back(false);
							}
						} else console.log("SQL ERROR save assigned_session: ", err);
					}
				);
			}
		})

		break;
	case "allocated_session":
		/*
			data_info = {
				* session_id
			}
		*/
		var session_update = new Date().toISOString();

		self.load_v5('session', {session_id:data_info.session_id}, function(session_info) {
			if (session_info) {
				self._mysql.m_c.query(
					"UPDATE `sessions` SET `session_state` = 'allocated', `session_update` = ? WHERE `session_id` = ? AND `session_state` IN ('unassigned', 'assigned') AND `session_status` != 'close'",
					[session_update, data_info.session_id],
					function(err, rows) {
						if (!err) {
							if (rows.changedRows > 0) {
								session_info['session_state'] = 'allocated';
								session_info['session_update'] = 'session_update';

								// Кешируем данные чата в редис
								self._redis.rc_out.info()(function* () {
									var chat_info = yield [
										this.hset("session:" + session_info['session_id'], "session_id", session_info['session_id']),
										this.hset("session:" + session_info['session_id'], "session_state", session_info['session_state']),
										this.hset("session:" + session_info['session_id'], "session_status", session_info['session_status']),
										this.hset("session:" + session_info['session_id'], "user_id", session_info['user_id']),
										this.hset("session:" + session_info['session_id'], "session_update", session_info['session_update']),
										this.hset("session:" + session_info['session_id'], "session_create", session_info['session_create']),
									];

									if (typeof call_back === 'function') call_back(session_info);
								})();
							} else {
								if (typeof call_back === 'function') call_back(false);
							}
						} else console.log("SQL ERROR save allocated_session: ", err);
					}
				);
			}
		});

		break;
//	case "close_session":
//		/*
//			data_info = {
//				* session_id
//			}
//		*/
//		var session_update = new Date().toISOString();
//		self._mysql.m_c.query(
//			"UPDATE `sessions` SET `session_status` = 'close', `session_update` = ? WHERE `session_id` = ? AND `session_status` != 'close'",
//			[session_update, data_info.session_id],
//			function(err, rows) {
//				if (!err) {
//					if (rows.changedRows > 0) {
//						if (typeof call_back === 'function') call_back({session_id:data_info.session_id, session_status:'close'});
//					} else {
//						if (typeof call_back === 'function') call_back(false);
//					}
//				} else console.log("SQL ERROR save close_session: ", err);
//			}
//		);
//
//		break;
	case "status_session":
		/*
			data_info = {
				* session_id
				* session_status
			}
		*/
		var session_update = new Date().toISOString();
		var list_status = ['active', 'pending', 'close'];
		if (!list_status.in_array(data_info.session_status)) return;

		self.load_v5('session', {session_id:data_info.session_id}, function(session_info) {
			if (session_info) {
				if (session_info.session_status == data_info.session_status) {
					if (typeof call_back === 'function') call_back(false);
				} else {
					self._mysql.m_c.query(
						"UPDATE `sessions` SET `session_status` = ?, `session_update` = ? WHERE `session_id` = ? AND `session_status` != 'close'",
						[data_info.session_status, session_update, data_info.session_id],
						function(err, rows) {
							if (!err) {
								if (rows.changedRows > 0) {
									session_info['session_status'] = data_info.session_status;
									session_info['session_update'] = session_update;

									// Кешируем данные чата в редис
									self._redis.rc_out.info()(function* () {
										var chat_info = yield [
											this.hset("session:" + session_info['session_id'], "session_id", session_info['session_id']),
											this.hset("session:" + session_info['session_id'], "session_state", session_info['session_state']),
											this.hset("session:" + session_info['session_id'], "session_status", session_info['session_status']),
											this.hset("session:" + session_info['session_id'], "user_id", session_info['user_id']),
											this.hset("session:" + session_info['session_id'], "session_update", session_info['session_update']),
											this.hset("session:" + session_info['session_id'], "session_create", session_info['session_create']),
										];

										self.load_v5('routing_message', {session_id:data_info.session_id}, function(routing_message) {
											if (routing_message) {
												session_info['routing_message'] = routing_message;
												if (typeof call_back === 'function') call_back(session_info);
											} else {
												session_info['routing_message'] = {};
												if (typeof call_back === 'function') call_back(session_info);
											}
										});
									})();
								} else {
									if (typeof call_back === 'function') call_back(false);
								}
							} else console.log("SQL ERROR save status_session: ", err);
						}
					);
				}
			}
		});

		break;
	case "message":
		/*
			data_info = {
				* session_id
				* user_id
				message_key
				message_type = 'text'
				message_state = 'received'
				* message_data = {}
			}
		*/

		if (typeof data_info.message_create === 'undefined') data_info.message_create = new Date().toISOString();
		data_info.message_update = data_info.message_create;

		var list_type = ['text', 'emotion', 'file', 'mention', 'product', 'order', 'advert', 'url', 'auto_vote', 'auto_greeting', 'auto_allocation', 'auto_offhours', 'btn_order', 'btn_lastseen', 'btn_basket'];
		if (!list_type.in_array(data_info.message_type)) data_info.message_type = 'text';

		var list_state = ['received', 'updated', 'deleted'];
		if (!list_state.in_array(data_info.message_state)) data_info.message_state = 'received';

		if (typeof data_info.session_id === 'undefined') return;
		if (typeof data_info.user_id === 'undefined') data_info.user_id = 0;
		if (typeof data_info.message_data !== 'object' || data_info.message_data === null) return;

		self._mysql.m_c.query(
			"INSERT INTO `messages` (`session_id`, `user_id`, `message_key`, `message_type`, `message_data`, `message_state`, `message_update`, `message_create`)  VALUES (?, ?, ?, ?, ?, ?, ?, ?)", 
			[data_info.session_id, data_info.user_id, data_info.message_key, data_info.message_type, JSON.stringify(data_info.message_data), data_info.message_state, data_info.message_update, data_info.message_create], 
			function(err, rows) {
				if (!err) {
					console.log("V5 Save message: ");
					if (typeof call_back === 'function') call_back({session_id:data_info.session_id, user_id:data_info.user_id, message_id:rows.insertId, message_key:data_info.message_key, message_type:data_info.message_type, message_data:data_info.message_data, message_state:data_info.message_state, message_update:data_info.message_update, message_create:data_info.message_create, delivmessagesusers:[]});
				} else console.log("SQL ERROR message: ", err);
			}
		);


		break;
	case "delivery":
		/*
			data_info = {
				* message_id
				* user_id
			}
		*/

		var delivmessageuser_create = new Date().toISOString();

		self._mysql.m_c.query(
			"SELECT * FROM `messages` WHERE `message_id` = ?", 
			[data_info.message_id], 
			function(err, m_rows) {
				if (!err) {
				        if (m_rows.length > 0) {
						self._mysql.m_c.query(
							"INSERT DELAYED INTO `delivmessagesusers` (`message_id`, `user_id`, `delivmessageuser_create`)  VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE `message_id`=`message_id`, `user_id`=`user_id`", 
							[data_info.message_id, data_info.user_id, delivmessageuser_create], 
							function(err, rows) {
								if (!err) {
									console.log("V5 Save delivery message: ");
									if (typeof call_back === 'function') call_back({session_id:m_rows[0].session_id, message_id:data_info.message_id, user_id:data_info.user_id, delivmessageuser_create:delivmessageuser_create});
								} else console.log("SQL ERROR delivery 2: ", err);
							}
						);
					}
				} else console.log("SQL ERROR delivery 1: ", err);
			}
		);

		break;
	case "routing":
		/*
			data_info = {
				* session_id
				* rule_id
			}
		*/
		self._mysql.m_c.query(
			"INSERT INTO `routings` (`session_id`, `rule_id`, `message_id`)  VALUES (?, ?, ?)",
			[data_info.session_id, data_info.rule_id, data_info.message_id], 
			function(err, rows) {
				if (!err) {
					console.log("V5 Save routing: ");
					if (typeof call_back === 'function') call_back({session_id:data_info.session_id, rule_id:data_info.rule_id, message_id:data_info.message_id});
				} else console.log("SQL ERROR routing: ", err);
			}
		);

		break;
	case "push":
		/*
			data_info = {
				* for_user_id

				* session_id
				?* message_id
				* user_id
				* message_key
				* message_type
				* message_data
				* message_state
				* message_update
				* message_create
			}
		*/
		self._mysql.m_c.query(
			"SELECT * FROM `mobiles` WHERE `id_user` = ?", 
			[data_info.for_user_id], 
			function(err, rows) {
				if (!err) {
					var dev_list = {};
					for(var d = 0; d < rows.length; d++) {
						if (typeof dev_list[rows[d].device_type] === 'undefined') dev_list[rows[d].device_type] = [];
						dev_list[rows[d].device_type].push(rows[d].device_token);
					}
        
					Object.keys(dev_list).forEach(function(v_type) {
						var v_tokens = dev_list[v_type];
						// добавляем пуш с указанием ключей
						self._mysql.m_c.query(
							"INSERT INTO `pushes` (`push_type`, `push_key_origin`, `push_data`, `push_state`)  VALUE(?, ?, ?, ?)", 
							[v_type, JSON.stringify(v_tokens), JSON.stringify({user_id:data_info.user_id, session_id:data_info.session_id, message_key:data_info.message_key, message_type:data_info.message_type, message_data:data_info.message_data, message_state:data_info.message_state, message_update:data_info.message_update, message_create:data_info.message_create}), 'Request'], 
							function(err, rows, fields) {
								if (!err) console.log('push created for ' + v_type);
								else console.log("SQL ERROR push: ", err);
							}
						);
					});

					console.log("V5 Save push sql: ");
					if (typeof call_back === 'function') call_back();
				} else console.log("SQL ERROR message: ", err);
			}
		);

		break;
	default:

	}
}

Storage.prototype.load_v5 = function(data_type, data_info, call_back) {
	var self = this;

	switch(data_type) {
	case "create_session":
		/*
			data_info = {
				*user_id
			}
		*/
		if (typeof data_info.user_id === 'undefined' || data_info.user_id <= 0) return;
		var session_create = new Date().toISOString();
		var session_update = session_create;

		self.load_v5('user', {user_id:data_info.user_id}, function(user_info) {
			data_info['session_state'] = 'unassigned';
			data_info['session_status'] = 'active';
			self._mysql.m_c.query(
				"INSERT INTO `sessions` (`user_id`, `session_state`, `session_status`, `session_update`, `session_create`) VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `session_id`=LAST_INSERT_ID(`session_id`)",
				[data_info.user_id, data_info.session_state, data_info.session_status, session_create, session_create],
				function(err, rows) {
					if (!err) {
						console.log("V5 Create session: ", rows);
        
						var session_id = rows.insertId;
						var session_state = data_info.session_state;
						var session_status = data_info.session_status;
						var user_id = data_info.user_id;
        
						// Кешируем данные чата в редис
						self._redis.rc_out.info()(function* () {
							var chat_info = yield [
								this.hset("session:" + session_id, "session_id", session_id),
								this.hset("session:" + session_id, "session_state", session_state),
								this.hset("session:" + session_id, "session_status", session_status),
								this.hset("session:" + session_id, "user_id", user_id),
								this.hset("session:" + session_id, "session_update", session_update),
								this.hset("session:" + session_id, "session_create", session_create),
							];
        
							// После создания чата добавляем в него владельца
							self.save_v5('relation', {session_id:session_id, user_id:user_id, sessionrelation_role:'owner', sessionrelation_state:'active'}, function(relation_info) {
								console.log("V5 Append owner: ", relation_info);
								relation_info['user_nick'] = user_info.user_nick;
								relation_info['user_role'] = user_info.user_role;
								if (typeof call_back === 'function') call_back({session_id:session_id, session_state:session_state, session_status:session_status, user_id:user_id, session_update:session_update, session_create:session_create, sessionsrelations:[relation_info], routing_message:{}});
							});
						})();
					} else console.log("SQL ERROR insert create_session: ", err);
				}
			);

		});

		break;
	case "sessions":
		/*
			data_info = {
				session_state:[]
			}
		*/
		self._mysql.m_c.query(
			"SELECT * from `sessions` " + ((typeof data_info.session_state !== 'undefined' && data_info.session_state.constructor === Array && data_info.session_state.length > 0) ? " WHERE `session_state` IN (" + data_info.session_state.map(function(cur_val) {return self._mysql.m_c.escape(cur_val);}).join(", ") + ")" : "") + " ORDER BY `session_update`",
			[], 
			function(err, rows) {
				if (!err) {
					if (rows.length <= 0) {
						console.log("V5 sql Load sessions: ", false);
						if (typeof call_back === 'function') call_back(false); 
					} else {
						console.log("V5 sql Load sessions: ", rows);
						if (typeof call_back === 'function') call_back(rows);
					}
				} else console.log("SQL ERROR load sessions: ", err);
			}
		);

		break;
	case "session":
		/*
			data_info = {
				* session_id
			}
		*/
		self._redis.rc_out.info()(function* () {
			var data = {};
			data['session_id'] = parseInt(yield this.hget("session:" + data_info.session_id, "session_id"));
			if (data['session_id'] > 0) {
				data['user_id'] = parseInt(yield this.hget("session:" + data['session_id'], "user_id"));
				data['session_state'] = yield this.hget("session:" + data['session_id'], "session_state");
				data['session_status'] = yield this.hget("session:" + data['session_id'], "session_status");
				data['session_update'] = yield this.hget("session:" + data['session_id'], "session_update");
				data['session_create']  = yield this.hget("session:" + data['session_id'], "session_create");

				console.log("V5 redis Load session: ", data);

				if (typeof call_back === 'function') call_back(data);
			} else {
				self._mysql.m_c.query(
					"SELECT * from `sessions` WHERE `session_id` = ?",
					[data_info.session_id], 
					function(err, rows) {
						if (!err) {
							if (rows.length <= 0) {
								console.log("V5 sql Load session: ", false);
								if (typeof call_back === 'function') call_back(false); 
							} else {
								console.log("V5 sql Load session: ", rows[0]);
								if (typeof call_back === 'function') call_back(rows[0]);
							}
						} else console.log("SQL ERROR load session: ", err);
					}
				);
			}
		})();

		break;
	case "open_sessions":
		/*
			data_info = {
				* user_id
			}
		*/
		self._redis.rc_out.info()(function* () {
			self._mysql.m_c.query(
				"SELECT * from `sessions` WHERE `user_id` = ? AND session_status != 'close'",
				[data_info.user_id], 
				function(err, rows) {
					if (!err) {
						if (rows.length <= 0) {
							console.log("V5 sql Load open session: ", false);
							if (typeof call_back === 'function') call_back(false); 
						} else {
							console.log("V5 sql Load open session: ", rows);
							if (typeof call_back === 'function') call_back(rows);
						}
					} else console.log("SQL ERROR load session: ", err);
				}
			);
		})();

		break;
	case "session_relations":
		/*
			data_info = {
				* session_id
				sessionrelation_state = ['']
			}
		*/
		self._redis.rc_out.info()(function* () {
			var session_relations = yield this.smembers("session:" + data_info.session_id + ":relations");
			if (session_relations.length > 0) {
				var list = [];
				for(var i = 0;i < session_relations.length; i++) {
					var info = session_relations[i].split("_");
					var user_id = info[0] == "U" ? parseInt(info[1]) : 0;
					var group_id = info[0] == "G" ? parseInt(info[1]) : 0;

					var sessionrelation_role = yield this.hget("relations:" + data_info.session_id + "_" + session_relations[i], "sessionrelation_role");
					var sessionrelation_state = yield this.hget("relations:" + data_info.session_id + "_" + session_relations[i], "sessionrelation_state");
					var sessionrelation_update = yield this.hget("relations:" + data_info.session_id + "_" + session_relations[i], "sessionrelation_update");
					var sessionrelation_create = yield this.hget("relations:" + data_info.session_id + "_" + session_relations[i], "sessionrelation_create");

					if (typeof data_info.sessionrelation_state !== 'undefined' && data_info.sessionrelation_state.constructor === Array && data_info.sessionrelation_state.length > 0) {
						for(var s = 0; s < data_info.sessionrelation_state.length; s++) {
							if (data_info.sessionrelation_state[s] == sessionrelation_state) list.push({session_id:data_info.session_id, user_id:user_id, group_id:group_id, sessionrelation_role:sessionrelation_role, sessionrelation_state:sessionrelation_state, sessionrelation_update:sessionrelation_update, sessionrelation_create:sessionrelation_create});
						}
					} else {
						list.push({session_id:data_info.session_id, user_id:user_id, group_id:group_id, sessionrelation_role:sessionrelation_role, sessionrelation_state:sessionrelation_state, sessionrelation_update:sessionrelation_update, sessionrelation_create:sessionrelation_create});
					}
				};

				console.log("V5 redis Load session relations: ", list);
				if (typeof call_back === 'function') call_back(list);
			} else {
				self._mysql.m_c.query(
					"SELECT * from `sessionsrelations` WHERE `session_id` = ?" + ((typeof data_info.sessionrelation_state !== 'undefined' && data_info.sessionrelation_state.constructor === Array && data_info.sessionrelation_state.length > 0) ? " AND `sessionrelation_state` IN (" + data_info.sessionrelation_state.map(function(cur_val) {return self._mysql.m_c.escape(cur_val);}).join(", ") + ")" : ""),
					[data_info.session_id], 
					function(err, rows) {
						self._redis.rc_out.info()(function* () {
							if (!err) {
								for(var i = 0; i < rows.length; i++) {
									if (rows[i].user_id > 0) {
										yield [
											this.sadd("user:" + rows[i].user_id + ":relations", rows[i].session_id),
											this.hset("relations:" + rows[i].session_id + "_U_" + rows[i].user_id, "sessionrelation_role", rows[i].sessionrelation_role),
											this.hset("relations:" + rows[i].session_id + "_U_" + rows[i].user_id, "sessionrelation_state", rows[i].sessionrelation_state),
											this.hset("relations:" + rows[i].session_id + "_U_" + rows[i].user_id, "sessionrelation_update", rows[i].sessionrelation_update),
											this.hset("relations:" + rows[i].session_id + "_U_" + rows[i].user_id, "sessionrelation_create", rows[i].sessionrelation_create),
										];
									} else if (rows[i].group_id > 0) {
										yield [
											this.sadd("group:" + rows[i].group_id + ":relations", rows[i].session_id),
											this.hset("relations:" + rows[i].session_id + "_G_" + rows[i].group_id, "sessionrelation_role", rows[i].sessionrelation_role),
											this.hset("relations:" + rows[i].session_id + "_G_" + rows[i].group_id, "sessionrelation_state", rows[i].sessionrelation_state),
											this.hset("relations:" + rows[i].session_id + "_G_" + rows[i].group_id, "sessionrelation_update", rows[i].sessionrelation_update),
											this.hset("relations:" + rows[i].session_id + "_G_" + rows[i].group_id, "sessionrelation_create", rows[i].sessionrelation_create),
										];
									}
								};

								console.log("V5 sql Load session relations: ", rows);
								if (typeof call_back === 'function') call_back(rows);
							} else console.log('SQL ERROR load session_relations: ', err);
						})();
					}
				);
			}
		})();

		break;
	case "user":
		/*
			data_info = {
				* user_id
			}
		*/
// Отключили кеш из за тестирования user_capacity
//		self._redis.rc_out.info()(function* () {
//			var user_id = parseInt(yield this.hget("user:" + data_info.user_id, "user_id"));
//			if (user_id > 0) {
//				var user_nick = yield this.hget("user:" + user_id, "user_nick");
//				var user_role = yield this.hget("user:" + user_id, "user_role");
//				var user_state = yield this.hget("user:" + user_id, "user_state");
//				var user_capacity = parseInt(yield this.hget("user:" + user_id, "user_capacity"));
//				var user_status = yield this.hget("user:" + user_id, "user_status");
//
//				console.log("V5 redis Load user:", {user_id:user_id, user_nick:user_nick, user_role:user_role, user_state:user_state, user_capacity:user_capacity, user_status:user_status});
//				if (typeof call_back === 'function') call_back({user_id:user_id, user_nick:user_nick, user_role:user_role, user_state:user_state, user_capacity:user_capacity, user_status:user_status});
//			} else {
				self._mysql.m_c.query(
					"SELECT `users`.* from `users` WHERE `users`.`user_id` = ?",
					[data_info.user_id], 
					function(err, rows) {
						if (!err) {
							self._redis.rc_out.info()(function* () {
								if (rows.length <= 0) {
									if (typeof call_back === 'function') call_back(false);
								} else {
									var data = {user_id:rows[0].user_id, user_nick:rows[0].user_nick, user_role:rows[0].user_role, user_state:rows[0].user_state, user_capacity:rows[0].user_capacity};
									data['user_status'] = yield this.hget("user:" + rows[0].user_id, "user_status");
									if (data['user_status'] === null) data['user_status'] = 'Logout';

									yield [
										this.hset("user:" + rows[0].user_id, "user_id", rows[0].user_id),
										this.hset("user:" + rows[0].user_id, "user_nick", rows[0].user_nick),
										this.hset("user:" + rows[0].user_id, "user_role", rows[0].user_role),
										this.hset("user:" + rows[0].user_id, "user_state", rows[0].user_state),
										this.hset("user:" + rows[0].user_id, "user_capacity", rows[0].user_capacity),
										this.hset("user:" + rows[0].user_id, "user_status", data['user_status']),
									];

									console.log("V5 sql Load user: ", data);
									if (typeof call_back === 'function') call_back(data);
								}
							})();
						} else console.log("SQL ERROR load user: ", err);
					}
				);
//			}
//		})();

		break;
	case "user_relations":
		/*
			data_info = {
				* user_id
				sessionrelation_role = ['']
				sessionrelation_state = ['']
			}
		*/

		self._redis.rc_out.info()(function* () {
			var user_relations = yield this.smembers("user:" + data_info.user_id + ":relations");
			if (user_relations.length > 0) {
				var list = [];
				for(var i = 0;i < user_relations.length; i++) {
					var session_id = parseInt(user_relations[i]);
					var group_id = 0;

					var sessionrelation_role = yield this.hget("relations:" + session_id + "_U_" + data_info.user_id, "sessionrelation_role");
					var sessionrelation_state = yield this.hget("relations:" + session_id + "_U_" + data_info.user_id, "sessionrelation_state");
					var sessionrelation_update = yield this.hget("relations:" + session_id + "_U_" + data_info.user_id, "sessionrelation_update");
					var sessionrelation_create = yield this.hget("relations:" + session_id + "_U_" + data_info.user_id, "sessionrelation_create");

					if (typeof data_info.sessionrelation_state !== 'undefined' && data_info.sessionrelation_state.constructor === Array && data_info.sessionrelation_state.length > 0) {
						for(var s = 0; s < data_info.sessionrelation_state.length; s++) {
							if (data_info.sessionrelation_state[s] == sessionrelation_state) list.push({session_id:session_id, user_id:data_info.user_id, group_id:group_id, sessionrelation_role:sessionrelation_role, sessionrelation_state:sessionrelation_state, sessionrelation_update:sessionrelation_update, sessionrelation_create:sessionrelation_create});
						}
					} else if (typeof data_info.sessionrelation_role !== 'undefined' && data_info.sessionrelation_role.constructor === Array && data_info.sessionrelation_role.length > 0) {
						for(var s = 0; s < data_info.sessionrelation_role.length; s++) {
							if (data_info.sessionrelation_role[s] == sessionrelation_role) list.push({session_id:session_id, user_id:data_info.user_id, group_id:group_id, sessionrelation_role:sessionrelation_role, sessionrelation_state:sessionrelation_state, sessionrelation_update:sessionrelation_update, sessionrelation_create:sessionrelation_create});
						}
					} else {
						list.push({session_id:session_id, user_id:data_info.user_id, group_id:group_id, sessionrelation_role:sessionrelation_role, sessionrelation_state:sessionrelation_state, sessionrelation_update:sessionrelation_update, sessionrelation_create:sessionrelation_create});
					}
				};

				console.log("V5 redis Load user_relations: ", list);
				if (typeof call_back === 'function') call_back(list);
			} else {
				self._mysql.m_c.query(
					"SELECT * from `sessionsrelations` WHERE `user_id` = ?" 
						+ ((typeof data_info.sessionrelation_state !== 'undefined' && data_info.sessionrelation_state.constructor === Array && data_info.sessionrelation_state.length > 0) ? " AND `sessionrelation_state` IN (" + data_info.sessionrelation_state.map(function(cur_val) {return self._mysql.m_c.escape(cur_val);}).join(", ") + ")" : "")
						+ ((typeof data_info.sessionrelation_role !== 'undefined' && data_info.sessionrelation_role.constructor === Array && data_info.sessionrelation_role.length > 0) ? " AND `sessionrelation_role` IN (" + data_info.sessionrelation_role.map(function(cur_val) {return self._mysql.m_c.escape(cur_val);}).join(", ") + ")" : ""),
					[data_info.user_id], 
					function(err, rows) {
						self._redis.rc_out.info()(function* () {
							if (!err) {
								for(var i = 0; i < rows.length; i++) {
									yield [
										this.sadd("user:" + rows[i].user_id + ":relations", rows[i].session_id),
										this.hset("relations:" + rows[i].session_id + "_U_" + rows[i].user_id, "sessionrelation_role", rows[i].sessionrelation_role),
										this.hset("relations:" + rows[i].session_id + "_U_" + rows[i].user_id, "sessionrelation_state", rows[i].sessionrelation_state),
										this.hset("relations:" + rows[i].session_id + "_U_" + rows[i].user_id, "sessionrelation_update", rows[i].sessionrelation_update),
										this.hset("relations:" + rows[i].session_id + "_U_" + rows[i].user_id, "sessionrelation_create", rows[i].sessionrelation_create),
									];
								};

								console.log("V5 sql Load user_relations: ", rows);
								if (typeof call_back === 'function') call_back(rows);
							} else console.log('SQL ERROR load user_relations: ', err);
						})();
					}
				);
			}
		})();

		break;
	case "user_relation":
		/*
			data_info = {
				* user_id
				* session_id
			}
		*/
		self.load_v5('user_relations', {session_id:data_info.session_id, user_id:data_info.user_id, sessionrelation_state:'active'}, function(user_relations) {
			if (user_relations.length <= 0) {
				console.log("V5 Load user_relation:", false);
				if (typeof call_back === 'function') call_back(false);
			} else {
				var user_relation = false;
				for(var i = 0; i < user_relations.length; i++) {
					var cur_relation = user_relations[i];
					if (cur_relation.session_id == data_info.session_id && cur_relation.user_id == data_info.user_id) {
						user_relation = cur_relation;
						break;
					}
				}
				console.log("V5 Load user_relation:", user_relation);
				if (typeof call_back === 'function') call_back(user_relation);
			}
/*
			if (!user_relations) return;

			var sessionrelation_role = undefined;
			var sessionrelation_state = undefined;
			var sessionrelation_update = undefined;
			var sessionrelation_create = undefined;
			for(var i = 0; i < user_relations.length; i++) {
				var cur_relation = user_relations[i];
				if (cur_relation.session_id == data_info.session_id && cur_relation.user_id == data_info.user_id) {
					sessionrelation_role = cur_relation.sessionrelation_role;
					sessionrelation_state = cur_relation.sessionrelation_state;
					sessionrelation_update = cur_relation.sessionrelation_update;
					sessionrelation_create = cur_relation.sessionrelation_create;
					break;
				}
			}
			console.log("V5 Load user_relation:", {session_id:data_info.session_id, user_id:data_info.user_id, sessionrelation_role:sessionrelation_role, sessionrelation_state:sessionrelation_state, sessionrelation_update:sessionrelation_update, sessionrelation_create:sessionrelation_create});
			if (typeof call_back === 'function') call_back({session_id:data_info.session_id, user_id:data_info.user_id, sessionrelation_role:sessionrelation_role, sessionrelation_state:sessionrelation_state, sessionrelation_update:sessionrelation_update, sessionrelation_create:sessionrelation_create});
*/
		});

		break;
	case "user_groups":
		/*
			data_info = {
				* user_id
			}
		*/
		self._mysql.m_c.query(
			"SELECT * from `groupsusers` WHERE `user_id` = ?",
			[data_info.group_id], 
			function(err, rows) {
				if (!err) {
					console.log("V5 sql Load user_groups:", rows);
					if (typeof call_back === 'function') call_back(rows);
				} else console.log('SQL ERROR load user_groups: ', err);
			}
		);

		break;
	case "groups":
		/*
			data_info = {}
		*/
		self._mysql.m_c.query(
			"SELECT * from `groups`",
			[], 
			function(err, rows) {
				if (!err) {
					var pending = rows.length;
					rows.forEach(function(a_item, a_key) {
						self._redis.rc_out.info()(function* () {
							yield [
								this.hset("group:" + a_item.group_id, "group_id", a_item.group_id),
								this.hset("group:" + a_item.group_id, "group_thumb", a_item.group_thumb),
								this.hset("group:" + a_item.group_id, "group_title", a_item.group_title),
								this.hset("group:" + a_item.group_id, "group_backup", a_item.group_backup),
								this.hset("group:" + a_item.group_id, "group_capacity", a_item.group_capacity),
							];
							if (0 === --pending) {
								console.log("V5 sql Load groups:", rows);
								if (typeof call_back === 'function') call_back(rows);
							}
						})();

					});
				} else console.log("SQL ERROR load groups: ", err);
			}
		);

		break;
	case "group":
		/*
			data_info = {
				* group_id
			}
		*/
// Временно отключил кеш, чтобы не мешал при тестировании, после надо добавить время жизни кеша.
//		self._redis.rc_out.info()(function* () {
//			var group_id = parseInt(yield this.hget("group:" + data_info.group_id, "group_id"));
//			if (group_id > 0) {
//				var group_thumb = yield this.hget("group:" + group_id, "group_thumb");
//				var group_title = yield this.hget("group:" + group_id, "group_title");
//				var group_backup = parseInt(yield this.hget("group:" + group_id, "group_backup"));
//				var group_capacity = parseInt(yield this.hget("group:" + group_id, "group_capacity"));
//
//				console.log("V5 redis Load group:", {group_id:group_id, group_thumb:group_thumb, group_title:group_title, group_backup:group_backup, group_capacity:group_capacity});
//				if (typeof call_back === 'function') call_back({group_id:group_id, group_thumb:group_thumb, group_title:group_title, group_backup:group_backup, group_capacity:group_capacity});
//			} else {
				self._mysql.m_c.query(
					"SELECT `groups`.* from `groups` WHERE `groups`.`group_id` = ?",
					[data_info.group_id], 
					function(err, rows) {
						if (!err) {
							self._redis.rc_out.info()(function* () {
								if (rows.length <= 0) {
									if (typeof call_back === 'function') call_back(false);
								} else {
									yield [
										this.hset("group:" + rows[0].group_id, "group_id", rows[0].group_id),
										this.hset("group:" + rows[0].group_id, "group_thumb", rows[0].group_thumb),
										this.hset("group:" + rows[0].group_id, "group_title", rows[0].group_title),
										this.hset("group:" + rows[0].group_id, "group_backup", rows[0].group_backup),
										this.hset("group:" + rows[0].group_id, "group_capacity", rows[0].group_capacity),
									];
									console.log("V5 sql Load group:", rows[0]);
									if (typeof call_back === 'function') call_back(rows[0]);
								}
							})();
						} else console.log("SQL ERROR load group: ", err);
					}
				);
//			}
//		})();

		break;
	case "group_relations":
		/*
			data_info = {
				* group_id
				sessionrelation_state = ['']
			}
		*/
		self._redis.rc_out.info()(function* () {
			var group_relations = yield this.smembers("group:" + data_info.group_id + ":relations");
			if (group_relations.length > 0) {
				var list = [];
				for(var i = 0;i < group_relations.length; i++) {
					var session_id = parseInt(group_relations[i]);
					var user_id = 0;

					var sessionrelation_role = yield this.hget("relations:" + session_id + "_G_" + data_info.group_id, "sessionrelation_role");
					var sessionrelation_state = yield this.hget("relations:" + session_id + "_G_" + data_info.group_id, "sessionrelation_state");
					var sessionrelation_update = yield this.hget("relations:" + session_id + "_G_" + data_info.group_id, "sessionrelation_update");
					var sessionrelation_create = yield this.hget("relations:" + session_id + "_G_" + data_info.group_id, "sessionrelation_create");

					if (typeof data_info.sessionrelation_state !== 'undefined' && data_info.sessionrelation_state.constructor === Array && data_info.sessionrelation_state.length > 0) {
						for(var s = 0; s < data_info.sessionrelation_state.length; s++) {
							if (data_info.sessionrelation_state[s] == sessionrelation_state) list.push({session_id:session_id, user_id:user_id, group_id:data_info.group_id, sessionrelation_role:sessionrelation_role, sessionrelation_state:sessionrelation_state, sessionrelation_update:sessionrelation_update, sessionrelation_create:sessionrelation_create});
						}
					} else {
						list.push({session_id:session_id, user_id:user_id, group_id:data_info.group_id, sessionrelation_role:sessionrelation_role, sessionrelation_state:sessionrelation_state, sessionrelation_update:sessionrelation_update, sessionrelation_create:sessionrelation_create});
					}
				};

				console.log("V5 redis Load group_relations:", list);
				if (typeof call_back === 'function') call_back(list);
			} else {
				self._mysql.m_c.query(
					"SELECT * from `sessionsrelations` WHERE `group_id` = ?" + ((typeof data_info.sessionrelation_state !== 'undefined' && data_info.sessionrelation_state.constructor === Array && data_info.sessionrelation_state.length > 0) ? " AND `sessionrelation_state` IN (" + data_info.sessionrelation_state.map(function(cur_val) {return self._mysql.m_c.escape(cur_val);}).join(", ") + ")" : ""),
					[data_info.group_id], 
					function(err, rows) {
						self._redis.rc_out.info()(function* () {
							if (!err) {
								for(var i = 0; i < rows.length; i++) {
									yield [
										this.sadd("group:" + rows[i].group_id + ":relations", rows[i].session_id),
										this.hset("relations:" + rows[i].session_id + "_G_" + rows[i].group_id, "sessionrelation_role", rows[i].sessionrelation_role),
										this.hset("relations:" + rows[i].session_id + "_G_" + rows[i].group_id, "sessionrelation_state", rows[i].sessionrelation_state),
										this.hset("relations:" + rows[i].session_id + "_G_" + rows[i].group_id, "sessionrelation_update", rows[i].sessionrelation_update),
										this.hset("relations:" + rows[i].session_id + "_G_" + rows[i].group_id, "sessionrelation_create", rows[i].sessionrelation_create),
									];
								};

								console.log("V5 sql Load group_relations:", rows);
								if (typeof call_back === 'function') call_back(rows);
							} else console.log('SQL ERROR load group_relations: ', err);
						})();
					}
				);
			}
		})();

		break;
	case "group_relation":
		/*
			data_info = {
				* group_id
				* session_id
			}
		*/
		self.load_v5('group_relations', {session_id:data_info.session_id, group_id:data_info.group_id}, function(group_relations) {
			if (!group_relations) return;

			var sessionrelation_role = undefined;
			var sessionrelation_state = undefined;
			var sessionrelation_update = undefined;
			var sessionrelation_create = undefined;
			for(var i = 0; i < group_relations.length; i++) {
				cur_relation = group_relations[i];
				if (cur_relation.session_id == data_info.session_id && cur_relation.group_id == data_info.group_id) {
					sessionrelation_role = cur_relation.sessionrelation_role;
					sessionrelation_state = cur_relation.sessionrelation_state;
					sessionrelation_update = cur_relation.sessionrelation_update;
					sessionrelation_create = cur_relation.sessionrelation_create;
					break;
				}
			}
			console.log("V5 Load group_relation:", {session_id:data_info.session_id, group_id:data_info.group_id, sessionrelation_role:sessionrelation_role, sessionrelation_state:sessionrelation_state, sessionrelation_update:sessionrelation_update, sessionrelation_create:sessionrelation_create});
			if (typeof call_back === 'function') call_back({session_id:data_info.session_id, group_id:data_info.group_id, sessionrelation_role:sessionrelation_role, sessionrelation_state:sessionrelation_state, sessionrelation_update:sessionrelation_update, sessionrelation_create:sessionrelation_create});
		});

		break;
	case "group_users":
		/*
			data_info = {
				* group_id
			}
		*/
		self._mysql.m_c.query(
			"SELECT * from `groupsusers` WHERE `group_id` = ?",
			[data_info.group_id], 
			function(err, rows) {
				if (!err) {
					console.log("V5 sql Load group_users:", rows);
					if (typeof call_back === 'function') call_back(rows);
				} else console.log('SQL ERROR load group_users: ', err);
			}
		);

		break;
	case "group_user":
		/*
			data_info = {
				* group_id
				* user_id
			}
		*/
		self.load_v5('group_users', {group_id:data_info.group_id}, function(group_users) {
			for(var i = 0; i < group_users.length; i++) {
				if (group_users[i].group_id == data_info.group_id && group_users[i].user_id == data_info.user_id) {
					console.log("V5 Check group_user:");
					if (typeof call_back === 'function') call_back(true);
					break;
				}
			}
			if (typeof call_back === 'function') call_back(false);
		});

		break;
	case "agents":
		/*
			data_info = {
			}
		*/
		self._mysql.m_c.query(
			"SELECT `users`.* FROM `users` WHERE `users`.`user_role` = 'agent'",
			[], 
			function(err, rows) {
				if (!err) {
					if (typeof call_back === 'function') call_back(rows);
				} else console.log('SQL ERROR load agents: ', err);
			} 
		);

		break;
	case "all_sessions":
		/*
			data_info = {
			}
// Надо переделать убрать всю инфу о пользователях в sessionsrelations 
		*/
		self._mysql.m_c.query(
			"SELECT `sessions`.* from `sessions`",
			[], 
			function(err, main_rows) {
				if (!err) {
					if (main_rows.length <= 0) {
						if (typeof call_back === 'function') call_back([]);
					} else {
						var pending = main_rows.length;
						main_rows.forEach(function(a_item, a_key) {
							self.load_v5('routing_message', {session_id:a_item.session_id}, function(routing_message) {
								if (routing_message) main_rows[a_key]['routing_message'] = routing_message;
								else main_rows[a_key]['routing_message'] = {};
        
								self._mysql.m_c.query(
									"SELECT `sessionsrelations`.*, `users`.`user_id`, `users`.`user_nick`, `users`.`user_role` from `sessionsrelations` LEFT JOIN `users` ON (`users`.`user_id` = `sessionsrelations`.`user_id` ) WHERE `sessionsrelations`.`sessionrelation_state` != 'leave' AND `sessionsrelations`.`session_id` = ?",
									[a_item.session_id], 
									function(err, second_rows) {
										if (!err) {
											main_rows[a_key]["sessionsrelations"] = second_rows;
											if (0 === --pending) {
												if (typeof call_back === 'function') call_back(main_rows);
											}
										} else console.log("SQL ERROR load all_session 2: ", err);
									}
								);
							});
						});
					}
				} else console.log("SQL ERROR load all_session 1: ", err);
			}
		);

		break;
	case "messages":
		/*
			data_info = {
				* session_create
			}
		*/
		self._mysql.m_c.query(
			"SELECT `messages`.* from `messages` WHERE `messages`.`session_id` = ? ORDER BY `messages`.`message_create`",
			[data_info.session_id], 
			function(err, massages_rows) {
				if (!err) {
					if (massages_rows.length <= 0) {
						if (typeof call_back === 'function') call_back([]);
					} else {
						var pending = massages_rows.length;
        
						massages_rows.forEach(function(a_item, a_key) {
							try {
								massages_rows[a_key].message_data = JSON.parse(massages_rows[a_key].message_data);
							} catch (e) {
								massages_rows[a_key].message_data = {};
							}
							self._mysql.m_c.query(
								"SELECT `user_id`,`delivmessageuser_create` from `delivmessagesusers` WHERE `message_id` = ?",
								[massages_rows[a_key].message_id], 
								function(err, second_rows) {
									if (!err) {
										massages_rows[a_key]["delivmessagesusers"] = second_rows;
										if (0 === --pending) {
											if (typeof call_back === 'function') call_back(massages_rows);
										}
									} else console.log("SQL ERROR load messages 2: ", err);
								}
							);
						});
					}
				} else console.log("SQL ERROR load messages 1: ", err);
			}
		);

		break;
	case "session_history":
		/*
			data_info = {
				* user_id
				session_create
			}
		*/
		if (typeof data_info.session_create === 'undefined') {
			self._mysql.m_c.query(
				"SELECT * from `sessions` WHERE `user_id` = ? ORDER BY `session_create` DESC LIMIT 1",
				[data_info.user_id], 
				function(err, session_rows) {
					if (!err) {
						if (session_rows.length <= 0) {
							if (typeof call_back === 'function') call_back({});
						} else {
							self.load_v5('routing_message', {session_id:data_info.session_id}, function(routing_message) {
								if (routing_message) session_rows[0]['routing_message'] = routing_message;
								else session_rows[0]['routing_message'] = {};

								self.load_v5('messages', {session_id:session_rows[0].session_id}, function(messages_list) {
									session_rows[0]["messages"] = messages_list;
									if (typeof call_back === 'function') call_back(session_rows[0]);
								});
							});
						}
					} else console.log("SQL ERROR load session_history: ", err);
				}
			);
		} else {
			self._mysql.m_c.query(
				"SELECT * from `sessions` WHERE `user_id` = ? AND UNIX_TIMESTAMP(`session_create`) < UNIX_TIMESTAMP(?) ORDER BY `session_create` DESC LIMIT 1",
				[data_info.session_create, data_info.user_id, data_info.session_create], 
				function(err, session_rows) {
					if (!err) {
						if (session_rows.length <= 0) {
							if (typeof call_back === 'function') call_back({messages:[]});
						} else {
							self.load_v5('messages', {session_id:session_rows[0].session_id}, function(messages_list) {
								session_rows[0]["messages"] = messages_list;
								if (typeof call_back === 'function') call_back(session_rows[0]);
							});
						}
					} else console.log("SQL ERROR load session_history: ", err);
				}
			);
		}

		break;
	case "change_state":
		/*
			data_info = {
				* user_id
				* user_state
			}
		*/

		var list_state = ['Online','Training','Lunch','O/B'];
		if (!list_state.in_array(data_info.user_state)) return;

		self._mysql.m_c.query(
			"UPDATE `users` SET `user_state` = ? WHERE `user_id` = ?",
			[data_info.user_state, data_info.user_id], 
			function(err, rows) {
				if (!err) {
					if (rows.changedRows > 0) {
						self._redis.rc_out.info()(function* () {
							var user_id = yield this.hget("user:" + data_info.user_id, "user_id");
							if (user_id > 0) {
								yield [
									this.hset("user:" + data_info.user_id, "user_state", data_info.user_state),
								];
							}
						})();
						if (typeof call_back === 'function') call_back({user_id:data_info.user_id, user_state:data_info.user_state});
					} else {
						if (typeof call_back === 'function') call_back(false);
					}
				} else console.log("SQL ERROR load change_state: ", err);
			}
		);

		break;
	case "routing":
		/*
			data_info = {
				* session_id:
			}
		*/
		self._mysql.m_c.query(
			"SELECT * from `routings` WHERE `session_id` = ?",
			[data_info.session_id], 
			function(err, rows) {
				if (!err) {
					if (rows.length > 0) {
						if (typeof call_back === 'function') call_back(rows[0]);
					} else {
						if (typeof call_back === 'function') call_back(false);
					}
				} else console.log("SQL ERROR load rules: ", err);
			}
		);

		break;
	case "rules":
		/*
			data_info = {
			}
		*/

		self._mysql.m_c.query(
			"SELECT * from `rules` WHERE `rule_state` = 'enabled' ORDER BY `rule_priority` DESC, `rule_create` DESC",
			[], 
			function(err, rows) {
				if (!err) {
					if (typeof call_back === 'function') call_back(rows);
				} else console.log("SQL ERROR load rules: ", err);
			}
		);

		break;
	case "rule_groupsusers":
		/*
			data_info = {
				* rule_id
			}
		*/
		self._mysql.m_c.query(
			"SELECT * from `rulesgroupsusers` WHERE `rule_id` = ?",
			[data_info.rule_id], 
			function(err, rows) {
				if (!err) {
					if (typeof call_back === 'function') call_back(rows);
				} else console.log("SQL ERROR load rule_relations: ", err);
			}
		);

		break;
	case "message":
		/*
			data_info = {
				* message_id
			}
		*/
		self._mysql.m_c.query(
			"SELECT * from `messages` WHERE `message_id` = ?",
			[data_info.message_id], 
			function(err, rows) {
				if (!err) {
					if (rows.length <= 0) {
						if (typeof call_back === 'function') call_back(false);
					} else {
						try {
							rows[0].message_data = JSON.parse(rows[0].message_data);
						} catch (e) {
							rows[0].message_data = {};
						}
						if (typeof call_back === 'function') call_back(rows[0]);
					}
				} else console.log("SQL ERROR load messages_session: ", err);
			}
		);

		break;
	case "agents":
		/*
			data_info = {}
		*/
		self._mysql.m_c.query(
			"SELECT * FROM `users` WHERE `user_role` = 'agent'",
			[], 
			function(err, rows) {
				if (!err) {
					if (typeof call_back === 'function') call_back(rows);
				} else console.log('SQL ERROR load agents: ', err);
			} 
		);

		break;
	case "managers":
		/*
			data_info = {}
		*/
		self._mysql.m_c.query(
			"SELECT * FROM `users` WHERE `user_role` = 'manager'",
			[], 
			function(err, rows) {
				if (!err) {
					if (typeof call_back === 'function') call_back(rows);
				} else console.log('SQL ERROR load managers: ', err);
			} 
		);

		break;
	case "routing_message":
		/*
			data_info = {
				* session_id
			}
		*/
		self._mysql.m_c.query(
			"SELECT `messages`.* from `routings` LEFT JOIN `messages` ON (`routings`.`message_id` = `messages`.`message_id` ) WHERE `routings`.`session_id` = ?",
			[data_info.session_id], 
			function(err, rows) {
				if (!err) {
					if (rows.length > 0) {
						if (typeof call_back === 'function') call_back(rows[0]);
					} else {
						if (typeof call_back === 'function') call_back(false);
					}
				} else console.log('SQL ERROR load routing_message: ', err);
			}
		);

		break;
	case "auto_message":
		/*
			data_info = {
				* automessage_type
				user_id
				group_id
				rule_id
			}
		*/
		if (typeof data_info.user_id === 'undefined') data_info.user_id = 0;
		if (typeof data_info.group_id === 'undefined') data_info.group_id = 0;
		if (typeof data_info.rule_id === 'undefined') data_info.rule_id = 0;

		var list_state = ['Rules','Allocation','Greeting','Pending'];
		if (!list_state.in_array(data_info.automessage_type)) return;

		self._mysql.m_c.query(
			"SELECT `message_type`,`message_data` FROM `automessages` WHERE `automessage_type` = ? AND `user_id` = ? AND `group_id` = ? AND `rule_id` = ?",
			[data_info.automessage_type, data_info.user_id, data_info.group_id, data_info.rule_id], 
			function(err, rows) {
				if (!err) {
					if (rows.length > 0) {
						try {
							rows[0].message_data = JSON.parse(rows[0].message_data);
						} catch (e) {
							rows[0].message_data = {};
						}
						rows[0]['message_create'] = new Date().toISOString();
						if (typeof call_back === 'function') call_back(rows[0]);
					} else {
						if (typeof call_back === 'function') call_back(false);
					}
				} else console.log('SQL ERROR load auto_message: ', err);
			} 
		);

		break;
	default:

	}

// настройки
// подбор подходящего агента согласно правилам
// возможно в all_chats надо отдавать все активные чаты, а закрытые чаты уже постранично
// также возможно хистори запросы можно переложить на АПИ, как минимум их нужно там продублировать
}

Storage.prototype.subscribe_user_v5 = function(user_id, call_back) {
 	var self = this;

	// Обрабатываем публикации приходящие по подпискам
	self._redis.rc_inc.on("message", function (channel, info) {
		data = JSON.parse(info);
		//console.log("V5 Income publish:", channel, data);
		self._socket.emit(data.publish_type, data);
	});

	// подписываем пользователя на свой канал
	self._redis.rc_inc.info()(function* () {
		var info = yield this.subscribe("user:" + user_id);
		console.log("V5 Subscribe user: " , user_id);
		if (typeof call_back === 'function') call_back(info);
	})();
}

Storage.prototype.unsubscribe_user_v5 = function(user_id, call_back) {
 	var self = this;

	self._redis.rc_inc.info()(function* () {
		var info = yield this.unsubscribe("user:" + user_id);
		console.log("V5 Unsubscribe user:", user_id);
		if (typeof call_back === 'function') call_back(info);
	})();
}

Storage.prototype.change_status_v5 = function(user_id, new_status, call_back) {
 	var self = this;

	self.load_v5('user', {user_id:user_id}, function(user_info) {
		if (user_info.user_status != new_status) {
			// Публикуем
			self._redis.rc_out.info()(function* () {
				// Устанавливаем новый статус
				yield this.hset("user:" + user_id, "user_status" , new_status);
				user_info["user_status"] = new_status;

				if (typeof call_back === 'function') call_back(user_info);
			})();
		} else if (typeof call_back === 'function') call_back(false);
	});
}

Storage.prototype.get_count_subscribers_v5 = function(user_id, call_back) {
 	var self = this;

	self._redis.rc_out.info()(function* () {
		var count_info = yield this.pubsub("numsub", "user:" + user_id);
		console.log("V5 get count subscribers:", count_info['user:' + user_id]);
		if (typeof call_back === 'function') call_back(count_info['user:' + user_id]);
	})();
}


// --------------- ФУНКЦИИ 5той ВЕРСИИ
