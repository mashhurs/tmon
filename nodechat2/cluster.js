var 	cluster = require('cluster'),
	config = require('./modules/config');

cluster.setupMaster({
    exec: "worker.js"
});

// Fork workers as needed.
for (var i = 0; i < config.NUM_WORKERS; i++) {
	var env = {port:config.START_PORT + i};
	newWorker = cluster.fork(env);
	newWorker.process.env = env;
}

cluster.on('exit', function(deadWorker, code, signal) {
	var env = deadWorker.process.env;
	newWorker = cluster.fork(env);
	newWorker.process.env = env;

	// Note the process IDs
	var newPID = newWorker.process.pid;
	var oldPID = deadWorker.process.pid;

	// Log the event
	console.log('die worker: ' + oldPID + ', create new: ' + newPID + ' in port: ' + newWorker.process.env['port']);
});