

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


INSTALLATION
------------


If you do not already have Composer installed, you may do so by following the instructions at getcomposer.org. On Linux and Mac OS X, you'll run the following commands:

~~~
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
~~~

If you had Composer already installed before, make sure you use an up to date version. You can update Composer by running 

~~~
composer self-update
~~~


With Composer installed, you can  run the following commands under a Web-accessible folder:

~~~
composer global require "fxp/composer-asset-plugin:~1.1.1"
git clone git@bitbucket.org:soldier_anna/tmon.git
~~~

The first command installs the composer asset plugin which allows managing bower and npm package dependencies through Composer. You only need to run this command once for all. The second command clones project's directory. 

Now you should get vendor directory by running 

~~~
composer update
~~~

### Database

Primarily create database.
Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=tmon',
    'username' => 'root',
    'password' => '1',
    'charset' => 'utf8',
];
```

Run next command under the project's directory to get db structure

~~~
php yii migrate
~~~

Now you should be able to access the application through the following URL, assuming `timon` is the directory
directly under the Web root.

~~~
http://localhost/timon/web/
~~~


Later you can get last version by running these commands

~~~
git pull origin master
php yii migrate
composer update
~~~


