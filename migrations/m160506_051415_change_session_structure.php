<?php

use yii\db\Migration;

class m160506_051415_change_session_structure extends Migration
{
    public function up()
    {
        $this->delete('sessions');
	$this->alterColumn('sessions', 'session_state', "ENUM('unassigned','assigned','allocated') NOT NULL DEFAULT 'unassigned' COMMENT '��� ������ (unassigned - ����� ��� �� ��������, assigned - ��������� � ������, allocated - ��������� ��������)'");
        $this->addColumn('sessions', 'session_status', "ENUM('active','pending','close') NOT NULL DEFAULT 'active' COMMENT '��������� (active - ������� ���������, pending - �������� �������, close - ������ �������)'");
    }

    public function down()
    {
        echo "m160506_051415_change_session_structure cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
