<?php

use yii\db\Schema;
use yii\db\Migration;

class m160305_085348_alter_chatrelations extends Migration
{
    public function up()
    {
        $this->alterColumn('chatsrelations', 'chatrelation_role', "ENUM('member', 'manager', 'owner') NOT NULL DEFAULT 'member' COMMENT 'Состояние (member - обычный участник, manager - управляющий, owner - владелец)'");
        $this->addColumn('messages', 'group_id', "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Группа из которой публиковал владельц' AFTER user_id");

    }

    public function down()
    {
        echo "m160305_085348_alter_chatrelations cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
