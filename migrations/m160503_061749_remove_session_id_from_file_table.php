<?php

use yii\db\Migration;

class m160503_061749_remove_session_id_from_file_table extends Migration
{
    public function up()
    {
	$this->dropColumn('files', 'session_id');
    }

    public function down()
    {
        echo "m160503_061749_remove_session_id_from_file_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
