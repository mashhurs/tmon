<?php

use yii\db\Schema;
use yii\db\Migration;

class m160318_112412_alter_messages extends Migration
{
    public function up()
    {
        $this->alterColumn('messages', 'message_type', "ENUM('text','emotion','file','mention', 'product', 'order', 'advert', 'url') NOT NULL DEFAULT 'text' COMMENT 'Разновидность сообщения/Тип данных (mention - упоминание)'" );

    }

    public function down()
    {
        echo "m160318_112412_alter_messages cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
