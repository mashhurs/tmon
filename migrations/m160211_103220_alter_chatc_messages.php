<?php

use yii\db\Schema;
use yii\db\Migration;

class m160211_103220_alter_chatc_messages extends Migration
{
    public function up()
    {
        $this->addColumn('chats', 'order_id', "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Привязка к ордеру' AFTER product_id");
        $this->addColumn('chats', 'group_id', "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Привязка к группе' AFTER order_id");

        $this->addColumn('messages', 'message_key', "VARCHAR(50) NOT NULL DEFAULT '' COMMENT 'Уникальный ключ заданный пользователем' AFTER message_id");
        $this->alterColumn('messages', 'message_type', "ENUM('text','emotion','file','mention', 'product', 'order', 'advert') NOT NULL DEFAULT 'text' COMMENT 'Разновидность сообщения/Тип данных (mention - упоминание)'");

    }

    public function down()
    {
        echo "m160211_103220_alter_chatc_messages cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
