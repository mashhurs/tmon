<?php

use yii\db\Migration;

class m160429_114548_rules_struct extends Migration
{
    public function up()
    {

        $this->delete('automessages');
	$this->alterColumn('automessages', 'automessage_type', "ENUM('Rules', 'Allocation', 'Greeting', 'Pending', 'OffHours') NOT NULL COMMENT '������������� ���� ��������� (Welkome - ��������������, Pending - � �������� ��������, Vote - ��������� ������ ������ ����� �� ��������) '");

        $this->batchInsert('automessages', ['automessage_id', 'user_id', 'group_id', 'rule_id', 'automessage_type', 'message_type', 'message_data', 'automessage_create'], [ 
            [1, 0, 0, 1, 'Rules', 'text', '{"text":"This is rules 1 !"}', '2016-02-01 00:00:00'],
            [2, 0, 0, 2, 'Rules', 'text', '{"text":"This is rules 2 !"}', '2016-02-01 01:00:00'],
            [3, 0, 0, 0, 'Allocation', 'allocation', '{"text":"This is Allocation !"}', '2016-02-01 05:00:00'],
            [4, 0, 0, 0, 'Greeting', 'greeting', '{"text":"This is Greeting !"}', '2016-02-01 05:00:00'],
            [5, 5, 0, 0, 'Greeting', 'greeting', '{"text":"This is Greeting for agent 5 !"}', '2016-02-01 05:00:00'],
            [6, 5, 0, 0, 'OffHours', 'offhours', '{"text":"Off hours time !"}', '2016-02-01 05:00:00'],
        ]);

        $this->delete('groupsusers');
        $this->batchInsert('groupsusers', ['groupuser_id', 'group_id', 'user_id', 'groupuser_create'], [ 
            [1, 1, 3, '2016-02-01 00:00:00'],
            [2, 1, 4, '2016-02-01 01:00:00'],
            [3, 1, 5, '2016-02-01 05:00:00'],
            [4, 2, 5, '2016-02-01 05:00:00'],
        ]);

        $this->delete('groups');
        $this->batchInsert('groups', ['group_id', 'group_thumb', 'group_title', 'group_backup', 'group_capacity', 'group_create'], [ 
            [1, '', 'Group 1', 0, 10, '2016-02-01 00:00:00'],
            [2, '', 'Group 2', 1, 20, '2016-02-01 01:00:00'],
            [3, '', 'Group 3', 4, 5, '2016-02-01 05:00:00'],
            [4, '', 'Group 4', 3, 50, '2016-02-01 05:00:00'],
        ]);

        $this->delete('rules');
        $this->batchInsert('rules', ['rule_id', 'user_id', 'group_id', 'rule_title', 'rule_info', 'rule_state', 'rule_priority', 'rule_create'], [ 
            [1, 0, 1, 'Example 1', '{"type":"OR","conditions":[{"key":"message_text", "value":"example", "strict":"complete"}, {"key":"message_text", "value":"test", "strict":"overlap"}]}', 'enabled', 30, '2016-02-01 00:00:00'],
            [2, 0, 2, 'Example 2', '{"type":"OR","conditions":[{"key":"device_type", "value":"android", "strict":"complete"}]}', 'enabled', 20, '2016-02-01 01:00:00'],
            [3, 3, 0, 'Example 3', '{"type":"AND","conditions":[{"key":"device_type", "value":"ios", "strict":"complete"}]}', 'enabled', 10, '2016-02-01 05:00:00']           
        ]);
    }

    public function down()
    {
        echo "m160429_114548_rules_struct cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
