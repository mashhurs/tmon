<?php

use yii\db\Schema;
use yii\db\Migration;

class m160314_102857_alter_chatsrelations_index extends Migration
{
    public function up()
    {
        $this->dropIndex('chat_relation_unique','chatsrelations');
        $this->createIndex('chat_relation_unique', 'chatsrelations', ['chat_id', 'user_id', 'group_id'], true);
    }

    public function down()
    {
        echo "m160314_102857_alter_chatsrelations_index cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
