<?php

use yii\db\Schema;
use yii\db\Migration;

class m160218_120824_alter_charts extends Migration
{
    public function up()
    {
        $this->createIndex('room_unique', 'chats', ['user_id', 'product_id', 'order_id', 'group_id'], true);
    }

    public function down()
    {
        echo "m160218_120824_alter_charts cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
