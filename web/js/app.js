/**
 * Created by Ayo on 01-Feb-16.
 */
/* custom
 ************/
(function(){
    'use strict';

    var tmon = angular.module('tmon', ['ngMaterial', 'btford.socket-io', 'uuid4', 'angularMoment']);
        tmon.config(function($mdThemingProvider) {
            $mdThemingProvider.definePalette('tmonPallete',{
                '50': 'f95a44',
                '100': 'f95a44',
                '200': 'f95a44',
                '300': 'f95a44', /*primary and warn*/
                '400': 'f95a44',
                '500': 'f95a44', /*primary and warn*/
                '600': 'f95a44',
                '700': 'f95a44',
                '800': 'f95a44', /*primary and warn*/
                '900': 'f95a44',
                'A100': 'f95a44', /*primary and warn*/
                'A200': 'f95a44', /*accent*/
                'A400': 'f95a44', /*accent*/
                'A700': 'f95a44' /*accent*/
            });
            $mdThemingProvider.theme('default')
                .primaryPalette('tmonPallete')
        });

    //Controller
    (function(){
        'use strict';
    
        tmon.controller('tmonCtrl', ['$scope', '$mdDialog', '$rootScope', 'tmonSocket', 'tmonServices', tmonCtrl]);
        function tmonCtrl($scope, $mdDialog, $rootScope, tmonSocket, tmonServices){
            //var for storing my msgkey
            $scope.myMsgKey = [];
            //$scope.all_chats_id = [];
            $scope.agent_id = agent_id;
            $scope.agent_token = user_token;
            $scope.agent_nick = agent_nick;
            $scope.all_chats = {
                chats: [],
                chats_id: [],
                curr_chat: {}
            };
            $scope.new_msg;
            $scope.msg_type = 'text';
            $scope.onMessageSend;
            $scope.show_emo = false;
            $scope.groupChatLength = 0;
            $scope.all_agents = [];
            $scope.all_clients = [];
            $scope.all_groups = [];
    
            $scope.hasHistory = function (){
                return !$scope.all_chats.curr_chat.hasNoHistory;
            }
    
            $scope.isManager = function(){
                return  !$scope.all_chats.curr_chat || !$scope.all_chats.curr_chat.type_group || $scope.all_chats.curr_chat.type_group != 'me';
            };
            $scope.isAssigned = function(){
                return  $scope.all_chats.curr_chat.type_group && $scope.all_chats.curr_chat.type_group != '' || $scope.all_chats.curr_chat.has_manager;
            };
            $scope.forward_info = false;
    
           
            $rootScope.$on('socket:all_chats_v3', function (a, data) {
                console.log('[All chats]', data);
                data.list_chats.map(function(chat){
                    addNewChat(chat);
                    for (var i = chat.chatsrelations.length - 1; i >= 0; i--) {
                        var cur = chat.chatsrelations[i];
                        defineUserRelation(chat.chat_id, cur);
                    }
                    //tmonSocket.emit('chat_relations_v3', {chat_id: chat.chat_id});
                });
            });
    
            function getChatById (chatId) {
                 for (var i = 0, length = $scope.all_chats.chats.length; i < length; i++) {
                    var chat = $scope.all_chats.chats[i];
                     if (chat.chat_id == chatId) {
                        return chat;
                     }
                 }
                 return null;
            }
            function getMsgById (chat, msgId) {
                if (chat.messages && chat.messages.mix) 
                for (var i = 0; i < chat.messages.mix.length; i++) {
                    var msg = chat.messages.mix[i];
                    if (msg.message_id == msgId) {
                        return msg;
                    }
                }
                return null;
            }
            function getNickById (userId) {
                var userInfo = tryUserInfo(userId);
                if (userInfo && userInfo.user_nick) {
                    return userInfo.user_nick;
                }
    
                if($scope.all_chats.curr_chat.chatsrelations)
                for (var i = 0; i < $scope.all_chats.curr_chat.chatsrelations.length; i++) {
                    var nick = $scope.all_chats.curr_chat.chatsrelations[i];
                    if (nick.user_id == userId) {
                        return nick.user_nick;
                    }
                }
                return null;
            }
            function addNewChat (chat) {  
                //addChatToTab(chat);
                $scope.all_chats.chats.push(chat);
                $scope.all_chats.chats_id.push(chat.chat_id.toString());
            }
    
            function addChatToTab (chat) {
                if (isUserInGroup(chat.group_id)) {
                    chat.type_group = 'group';
                }
                if (isUserManagerOfChat(chat)) {
                    chat.type_group = 'me';
                }
            }
    
            function isChatExist (chat) {
                 for (var i = 0; i < $scope.all_chats.chats.length; i++) {
                     var ch = $scope.all_chats.chats[i];
                     if (ch.chat_id == chat.chat_id) {
                        return true;
                     }
                 }
                 return false;
            }
    
            function isUserInGroup(groupId){
                for (var i = 0; i < groups.length; i++) {
                    var group = groups[i];
                    if (group.group_id == groupId) {
                        return true;
                    }
                }
                return false;
            }
    
            $rootScope.$on('socket:chat_relations_v3', function (a, data) {
                console.log('[Chat Relations V3]', data);
                data.relations.map(function(member){               
                    defineUserRelation(member.chat_id, member);
                });
            });
    
    
            $scope.initMemberInfo = function (member){
                if (member.user_id && !member.user_nick) {
                    var userInfo = tryUserInfo(member.user_id);
                    if (userInfo) {
                        angular.extend(member, userInfo);
                    }
                }
            }
    
            function defineUserRelation(chatId, chatrelation){
                var cur = getChatById(chatId);
                if (chatrelation.user_id && !chatrelation.user_nick) {
                    queryUserInfo(chatrelation.user_id);
                }
    
                if (chatrelation.chatrelation_state == "active") {
    
                        switch (chatrelation.chatrelation_role) {
                            case 'member':
                                // if (isUserInGroup(cur.group_id) || (!chatrelation.user_id && isUserInGroup(chatrelation.group_id))) {
                                //     cur.type_group = 'group';
                                // }
                                if (!chatrelation.user_id && chatrelation.group_id) {
                                    if (isUserInGroup(chatrelation.group_id)) {
                                        cur.type_group = 'group';
                                    } else {
                                        cur.has_manager = true;                                    
                                    }
                                }
    
    
                                break;
                            case 'manager':
                                cur.has_manager = true;
                                if (chatrelation.user_id == agent_id) {
                                    cur.type_group = 'me';
                                }
                                break;
                            default:
                                // statements_def
                                break;
                        }
    
    
                }
                var memberExists = false;
                if (!cur.chatsrelations) {
                    cur.chatsrelations = [];
                }
                for (var i = 0; i < cur.chatsrelations.length; i++) {
                    var m = cur.chatsrelations[i];
                    if (chatrelation.user_id == m.user_id) {
                        memberExists = true;
                        break;
                    }
                }
                if (!memberExists)
                    cur.chatsrelations.push(chatrelation);
            }
    
    
            function queryUserInfo(user_id){
                if (!tryUserInfo(user_id)) { 
                    tmonSocket.emit('user_info_v3', {user_id: user_id});
                }
            }
    
            function tryUserInfo(user_id){
                var result = null;
                for (var i = $scope.all_agents.length - 1; i >= 0; i--) {
                    var agent = $scope.all_agents[i];
                    if (agent.user_id == user_id) {
                        result = agent;
                        break;
                    }
                }
                if(!result)
                for (var i = $scope.all_clients.length - 1; i >= 0; i--) {
                    var client = $scope.all_clients[i];
                    if (client.user_id == user_id) {
                        result = client;
                        break;
                    }
                }
    
                return result;
    
            }
    
            $rootScope.$on('socket:user_info_v3', function (a, data) {
                console.log('[User Info V3]', data);
                if (data.user_info) {
                    if (data.user_info.user_role == 'customer') {
                    $scope.all_clients.push(data.user_info);
                
                    if(data.user_info.user_role == 'agent'){
                        $scope.all_agents.push(data.user_info);
                    }    
                }
            }
                
            });
    
            function addMsgToChat(cur, msg, start){
    
                msg.message_create_date = new Date(msg.message_create);
                if (!cur.messages) {
                    cur.messages = {};
                }
    
                if (!cur.messages.mix) {
                    cur.messages.mix = [];
                }
    
                var contain = false;
                for (var i = 0; i < cur.messages.mix.length; i++) {
                    var tmp = cur.messages.mix[i];
                    if (tmp.message_id == msg.message_id) {
                        contain = true;
                        break;
                    }
                }
    
                if (!contain) {
                    if (start) {
                        cur.messages.mix.unshift(msg);
    
                    }else{
                        cur.messages.mix.push(msg);
                    }
                    
                }
    
                    if (!msg.user_nick) {
                        msg.user_nick = getNickById(msg.user_id);
                    }
            }
    
            $rootScope.$on('socket:chat_history_v3', function (a, data) {
                console.log('[Chat History V3]: ', data);
                
                var chat = getChatById(data.chat_id);
                if (chat.type_group == 'me' || chat.type_group == 'group' || chat.has_manager) {
                    chat.cached = true;
                }
                if (!data.later || data.later.length == 0) {
                    chat.hasNoHistory = true;
                    return;
                }
                if (data.later.length < 10) {
                    chat.hasNoHistory = true;                
                }
    
                mapChatHistory(chat, data);
    
                var sorted = data.later.sort(function(a,b){
                    return a.message_create_date == b.message_create_date ? 0 : a.message_create > b.message_create ? 1: -1;  
                })
                // TODO remove last_msg_date
                chat.messages.last_msg_date = sorted[data.later.length -1].message_create;
    
    
                rotate('.bar');
    
            });
            function mapChatHistory(chat, data){
                data.later.map(function (msg) {
                    // check msg delivery status and manage it appropriately
                    if(msg.delivmessagesusers.length){
                        if(msg.delivmessagesusers.length > 1 && msg.user_id == agent_id){
                            msg.delivered = true;
                        }else{
                            tmonServices.messageDelivery(msg.message_id);
                        }
                    }else{
                        msg.delivered = false;
                    }
                    addMsgToChat(chat, msg);
                });
                //return tmix;
            }
            function setChatTypeGroup (chats, type_group) {
                  for (var i = 0, l = chats.length; i < l; i++) {
                        var chat = chats[i];
                        var chatFromAll = getChatById(chat.chat_id);
                        if (!chatFromAll) {
                            $scope.all_chats.chats.push(chat);
                            chatFromAll = chat;
                        }
                        chatFromAll.type_group = type_group;
                    }
            }
            $rootScope.$on('socket:user_chats_v3', function (a, data) {
                console.log('[User Chats]', data);
                if (data.user_id == agent_id) {
                    //for (var i = 0; i < data.list_chats.length; i++) {
                    //    var с = data.list_chats[i];
                    //    tmonSocket.emit('chat_relations_v3', {chat_id: с.chat_id});
                    //}
                   setChatTypeGroup(data.list_chats, 'me');
                }
            });
            $rootScope.$on('socket:group_chats_v3', function (a, data) {
                console.log('[Group Chats]', data);
                if (isUserInGroup(data.group_id)) {
                    setChatTypeGroup(data.list_chats, 'group');
                }
            });
            $rootScope.$on('socket:empty_chats_v3', function (a, data) {
                console.log('[Empty Chats]', data);
                setChatTypeGroup(data.list_chats, 'empty');
            });
    
            $rootScope.$on('socket:assign_manager_v3', function (a, data) {  
                console.log('[Assign Manager]', data);        
                var mustAdd = false;
                var chat = getChatById(data.chat_id);
                if (!chat) {
                    mustAdd = true;
                    chat = {
                        chat_id: data.chat_id,
                        chatsrelations: []
                    };
                }
                if (data.manager_info) {
                    chat.type_group = '';
                    if (data.manager_info.group_id && isUserInGroup(data.manager_info.group_id)) {
                        chat.type_group = 'group';
                    }
                    if (data.manager_info.user_id && data.manager_info.user_id == agent_id) {
                        chat.type_group = 'me';
                    }
                    chat.group_id = data.manager_info.group_id;
                    chat.agent_id = data.manager_info.user_id;
    
                }
                // else{
                //     //chat.type_group = 'empty';
                // }
                tmonSocket.emit('chat_relations_v3', {chat_id: chat.chat_id});
                if (mustAdd) {
                    addNewChat(chat);
                }
                // else{
                //     addChatToTab(chat);
                // }
            });
    
            $rootScope.$on('socket:relation_v3', function (a, data) {
                console.log('[Relation_v3]', data);
                if(data.chatrelation_state == "leave" && data.user_id == agent_id){
                    console.log('[Chat Leave]',data);
                    var chat = getChatById(data.chat_id);
                    chat.type_group = '';
                    if (chat.chat_id == $scope.all_chats.curr_chat.chat_id) {
                        $scope.all_chats.curr_chat = false;
                        //$scope.hasHistory = false;
                    }
    
                }
                if (data.chatrelation_state == 'typing') {
                    tmonServices.styleTyping(data);
                    return;
                }
                if((data.group_id == $scope.forward_info.group_id || data.user_id == $scope.forward_info.user_id) && data.chat_id == $scope.forward_info.chat_id){
                    $scope.afterForward(data);
                }
                var chat = getChatById(data.chat_id);
                if(!chat) {
                    addNewChat(data);                
                    $('.tabs-chat-'+data.chat_id+' .flex-xs-100 span.dn').addClass('unread');
                }
                var tabbed = false;
                if (isUserInGroup(data.group_id)) {
                    tabbed = true;
                    chat.type_group = 'group';
                }
                if (data.user_id == agent_id) {
                    if (data.chatrelation_role == "manager" && data.chatrelation_state == "active") {
                        tabbed = true;
                        chat.type_group = 'me';
                    }
                    if (tabbed) {
                        tmonSocket.emit('chat_history_v3', {chat_id: chat.chat_id, user_id: agent_id});
                    }else{
                        if(data.chatrelation_role != "member")
                            chat.type_group = null;
    
                    }
                }
                
                tmonServices.styleTyping(data);
    
            });
    
            $rootScope.$on('socket:message_v3', function (a, data) {
                console.log('[New Message]', data);
                $scope.client_msg = data;
                $scope.showMsg(data);
            });
    
            $rootScope.$on('socket:message_delivery_v3', function (a, data) {
                console.log('[Message Delivered]', data);
                var chat = getChatById(data.chat_id)
                if(chat){
                    var msg = getMsgById(chat, data.message_id);
                    if (msg) {
                        deliveryStatusTrue(msg);
                    }
                }
            });
    
            //getting all chats
            $scope.getAllChats = function(){
                tmonSocket.emit('all_chats_v3', {});
            };
            //getting me chats
            $scope.getMeChats = function(){
               tmonSocket.emit('user_chats_v3', {user_id:agent_id});
            };
            //getting empty chats
            $scope.getEmptyChats = function(){
               tmonSocket.emit('empty_chats_v3', {});
            };
            //getting group chats
            $scope.getGroupChats = function(){
                for (var i = 0; i < groups.length; i++) {
                    var group = groups[i];
                   tmonSocket.emit('group_chats_v3', {group_id: group.group_id});
                }
            };
    
            
            $scope.getMembers = function(data){
                console.log('getMembers', data);
                data.relations.map(function(member){
                    tmonSocket.emit('user_info_v3', {user_id: member.user_id});
                });
            };
            $scope.selectChat = function(chat, $index){
                console.log('[Select Chat]', chat);
                //$scope.hasHistory = true;
                if(chat.cached != true){
    
                    //set parametrs
                    $scope.all_chats.chats[$index] = chat;
                    $scope.all_chats.curr_chat = chat;
                    $scope.all_chats.curr_chat.index = $index;
    
                    //request for history and chat members
                    tmonSocket.emit('chat_history_v3', {chat_id: chat.chat_id, user_id: agent_id});
                    
                    tmonServices.scrollBottom();
                }else{
                    if($scope.all_chats.curr_chat.chat_id != chat.chat_id) {
                        $scope.all_chats.curr_chat = chat;
                    }
                    tmonServices.scrollBottom();
                }
                removeUnread(chat);
                //setDelivery(chat);
                //console.log('[all chats]:', $scope.all_chats);
            };
    
            //sending message
            $scope.sendMsg = function(new_msg){
                console.log('[Send Msg]', new_msg);
                if(new_msg){
                    // if($scope.msg_type == 'text'){
                    //     var url = '';
                    //     var arr = new_msg.split(' ');
                    //     for (var i = arr.length - 1; i >= 0; i--) {
                    //         var tmp = findUrls(arr[i]);
                    //         if(tmp.length > 0){
                    //             url = tmp[0];
                    //             break;
                    //         }
                    //     }
                         
                    //     if (url){
                            
                    //         if (url.lastIndexOf('.png') == 0 || url.lastIndexOf('.jpeg') == 0 || url.lastIndexOf('.jpg') == 0 || url.lastIndexOf('.gif') == 0){
                    //             $scope.msg_type = 'img';
                    //             $scope.sendMsg({chat_id:$scope.all_chats.curr_chat.chat_id, message_type: $scope.msg_type, message_data:{text:new_msg,image:url},message_key:tmonServices.genuuid()});
    
                    //         }else{
                    //             $scope.msg_type = 'url';
                    //             tmonServices.getUrlInfo(url).then(function(data){
                    //                 $scope.sendMsg({chat_id:$scope.all_chats.curr_chat.chat_id, message_type: $scope.msg_type, message_data:{text:new_msg,title:data.title,image:data.metas['og:image'],description:data.metas['og:description']},message_key:tmonServices.genuuid()});
    
                    //             }).catch(function(data){
                    //                 console.log('[Get Url Info Error]', data);
                    //             });
                    //         }
                    //         return;
    
                    //     }
                    // }
                    var data = {};
                    if (typeof($scope.onMessageSend) == "function") {
                        $scope.onMessageSend();
                    }
                    if ($scope.msg_type == 'url' || $scope.msg_type == 'img') {
                        data = new_msg;
                    } else
                    if($scope.msg_type == "emotion"){
                        console.log('emotions type msg');
                        data = {chat_id:$scope.all_chats.curr_chat.chat_id, message_type: $scope.msg_type, message_data:{text:new_msg.emotion_url,title:new_msg.emotion_title,id:new_msg.emotion_id},message_key:tmonServices.genuuid()};
                    }else{
                        data = {chat_id:$scope.all_chats.curr_chat.chat_id, message_type: $scope.msg_type, message_data:{text:new_msg},message_key:tmonServices.genuuid()};
                    }
                    console.log('data;',data);
                    tmonSocket.emit('user_message_v3', data);
                    tmonSocket.emit('user_relation_v3', {chat_id: $scope.all_chats.curr_chat.chat_id, user_id: $scope.agent_id, chatrelation_state:'active'});
                    console.log('[sending message]', data);
    
                    $scope.new_msg = '';
                    tmonServices.scrollBottom();
                    $scope.msg_type = 'text';
                    $scope.onMessageSend = null;
                }
            };
    
            function addUnread (chat_id, count) {
                 var chat = getChatById(chat_id);
                    if(!chat.unread){
                        chat.unread = count;
                        $('.tabs-chat-'+ chat_id+' .flex-xs-100 span.dn').addClass('unread');
                    }else{
                        chat.unread += count;
                    }
            }
            function removeUnread (chat) {
                console.log('unread chat1', chat);
                // also change delivery status
                 chat.unread = null;
                 $('.tabs-chat-'+ chat.chat_id+' .flex-xs-100 span.dn').removeClass('unread');
                //setDelivery(chat);
            }
            function setDelivery(chat){
                chat.messages.mix.map(function(msg){
                    if(msg.delivered==false){
                        tmonServices.messageDelivery(msg.message_id);
                        msg.delivered = true;
                    }
                });
            }
    
            function findUrls( text )
            {
                var source = (text || '').toString();
                var urlArray = [];
                var url;
                var matchArray;
    
                // Regular expression to find FTP, HTTP(S) and email URLs.
                var regexToken = /(([a-z]+:\/\/)?(([a-z0-9\-]+\.)+([a-z]{2}|aero|arpa|biz|com|coop|edu|gov|info|int|jobs|mil|museum|name|nato|net|org|pro|travel|local|internal))(:[0-9]{1,5})?(\/[a-z0-9_\-\.~]+)*(\/([a-z0-9_\-\.]*)(\?[a-z0-9+_\-\.%=&amp;]*)?)?(#[a-zA-Z0-9!$&'()*+.=-_~:@/?]*)?)(\s+|$)/gi;
    
                // Iterate through any URLs in the text.
                while( (matchArray = regexToken.exec( source )) !== null )
                {
                    var token = matchArray[0];
                    urlArray.push( token );
                }
    
                return urlArray;
            }
    
    
            // working with new messages
            $scope.showMsg = function(msg){
                msg.delivered = false;
                msg.user_nick = getNickById(msg.user_id);
                //adding new message coming from the server
                if(msg.message_type == "mention"){
                    
                }
                var cur = getChatById(msg.chat_id);
    
                addMsgToChat(cur, msg, true);
    
                // if currChatId == new msg id
                // show the message in the open chat
                if($scope.all_chats.curr_chat.chat_id == msg.chat_id){
                    // also confirm message delivery
                    if(msg.user_id != agent_id){
                        tmonServices.messageDelivery(msg.message_id);
                    }
                    tmonServices.scrollBottom();
                }else{
                    //else show the count of new messages
                   addUnread(msg.chat_id, 1);
                }
            };
            var typing = false;
            $scope.sendTyping = function(){
                tmonServices.sendTyping(typing, $scope.all_chats.curr_chat.chat_id, $scope.agent_id);
            };
    
            $scope.getHistory = function(){
                tmonSocket.emit('chat_history_v3', {chat_id: $scope.all_chats.curr_chat.chat_id, message_create: $scope.all_chats.curr_chat.messages.last_msg_date});
            };
            $scope.leaveChat = function(id){
                console.log('leaving',id);
                $scope.msg_type = 'mention';
                var new_msg = agent_nick + " closed conversation at "+dateParse();
                $scope.sendMsg(new_msg);
                tmonSocket.emit('user_relation_v3', {chat_id: id, user_id:agent_id, chatrelation_state: "leave"});
            };
            $scope.moreDesc = function(){
                $('.txt-desc').toggleClass('full-desc');
            };
            
            $scope.dl_type = 'forward';
            $scope.showForwardDialog = function($event) {
                $scope.dl_type = 'forward';
                showDialog();
            };
            $scope.showMemoDialog = function($event) {
                $scope.dl_type = 'memo';
                showDialog();
               
            };
            function deliveryStatusTrue(msg){
                msg.delivered = true;
            }
            function showDialog(){
                 $mdDialog.show({
                    controller: modalCtrl,
                    templateUrl: './js/agentModal.tmpl.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose:true,
                    scope: $scope,
                    preserveScope: true
                })
            }
            function modalCtrl($scope, $mdDialog){
                $scope.closeDialog = function(){
                    $mdDialog.hide();
                };
                $scope.cancel = function() {
                    $mdDialog.cancel();
                };         
    
                
    
    
                //forwarding chat to the another agent
                var fwAgent = function(userid, usernick, chatid){
                    var isMember = false;
                    console.log('curChatid', chatid);
                     $scope.all_chats.curr_chat.chatsrelations.map(function(member){
                         if(userid == member.user_id){
                             isMember = true;
                         }
                     });
                     if(!isMember){
                         $scope.forward_info = {
                             user_nick: usernick,
                             user_id: userid,
                             chat_id: chatid
                         };
                         tmonServices.fwAgent(userid,chatid);
                         $scope.cancel();
                     }else{
                         $mdDialog.show(
                             $mdDialog.alert()
                                 .parent(angular.element(document.body))
                                 .clickOutsideToClose(true)
                                 .title('Canceled!')
                                 .textContent(usernick + ' is already a member of current chat.')
                                 .ariaLabel('Canceled')
                                 .ok('Ok!')
                         );
                     }
                };
    
    
                //forwarding chat to the another group
                var fwGroup = function(groupid, group_title, chatid){
                    var isMember = false;
                    console.log('curChatid', chatid);
                    $scope.all_chats.curr_chat.chatsrelations.map(function(member){
                        if(groupid == member.group_id){
                            isMember = true;
                        }
                    });
                    if(!isMember){
                        $scope.forward_info = {
                            group_title: group_title,
                            group_id: groupid,
                            chat_id: chatid
                        };
                        //console.log('curr', chatid);
                        tmonServices.fwGroup(groupid,chatid);
                        $scope.cancel();
                    }else{
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Canceled!')
                                .textContent(group_title + ' is already a member of current chat.')
                                .ariaLabel('Canceled')
                                .ok('Ok!')
                        );
                    }
                };
    
                var memoAgent = function(userid, usernick, chatid){
                    $scope.msg_type = 'mention';
                    $scope.onMessageSend = function(){
                        tmonSocket.emit('user_relation_v3', {chat_id: chatid, user_id: userid, chatrelation_state:'active'});
                    };
                    console.log('mememmooo agent after onMsgSend');
                    $scope.new_msg = ($scope.new_msg ? $scope.new_msg + " @"+usernick : "@"+usernick)+" ";
                    $scope.cancel();
    
                };
                var memoGroup = function(groupid, grouptitle, chatid){
                    $scope.msg_type = 'mention';
                    $scope.onMessageSend = function(){
                        tmonSocket.emit('user_relation_v3', {chat_id: chatid, group_id: groupid, chatrelation_state:'active'});
                    };
                    $scope.new_msg = ($scope.new_msg ? $scope.new_msg + " @"+grouptitle : "@"+grouptitle)+" ";
                    $scope.cancel();
                };
    
                $scope.selectAgent = $scope.dl_type == 'forward' ? fwAgent : memoAgent;
                $scope.selectGroup =  $scope.dl_type == 'forward' ? fwGroup : memoGroup;
            }
    
            $scope.afterForward = function(data){
                if(typeof $scope.forward_info.user_nick === "undefined"){
                    var receiver = $scope.forward_info.group_title;
                }else{
                    var receiver = $scope.forward_info.user_nick;
                }
                console.log('forwarded agent data',data);
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.body))
                        .clickOutsideToClose(true)
                        .title('Success!')
                        .textContent('Chat has been successfully forwarded to ' + receiver + '!')
                        .ariaLabel('Forwarded')
                        .ok('Ok!')
                );
                $scope.msg_type = 'mention';
                var new_msg =  "Conversation has been forwarded to " +receiver;
                $scope.sendMsg(new_msg);
               // $scope.leaveChat($scope.forward_info.chat_id);
    
               tmonServices.becomeMember($scope.forward_info.chat_id);
                $scope.forward_info = false;
            };
            $scope.assignChat = function(ev, chatid) {
                // Appending dialog to document.body to cover sidenav in docs app
                var confirm = $mdDialog.confirm()
                    .title('Assigning to the chat')
                    .textContent('Would you like to assign the chat to yourself?')
                    .ariaLabel('AssignToYourself')
                    .targetEvent(ev)
                    .ok('Assign')
                    .cancel('Cancel');
                $mdDialog.show(confirm).then(function() {
                    console.log('assigning to yourself');
                    tmonServices.fwAgent(agent_id,chatid);
                    $scope.msg_type = 'mention';
                    var new_msg = agent_nick +" assigned conversation at "+dateParse();
                    $scope.sendMsg(new_msg);
                }, function() {
                    console.log('canceled');
                });
            };
            function dateParse(){
                var d = new Date().getDate();
                var m = new Date().getMonth() + 1;
                var day = ("0" + d).slice(-2);
                var month = ("0" + m).slice(-2);
                var year = new Date().getFullYear();
                return year + '.' + month + '.' + day;
            }
            $scope.showEmoBar = function(val){
                !val ? $scope.show_emo = true : $scope.show_emo = false;
            };
            $scope.sendEmo = function(emo){
                console.log('send');
                $scope.msg_type = 'emotion';
                $scope.sendMsg(emo);
            };
    
    
            function onStart() {
    
                if (window.location.hostname == 'localhost') {
                    var agentdata = {
                                        "status":true,
                                        "list":[
                                            {"user_id":"3","user_login":"agent1","user_pass":"64c001fc0366f8b8339271fc342a36db80028be1","auth_key":"MLgkXQyVc8O3pPqjZ3xKaKaYuVwBxn2c","password_reset_token":"","user_nick":"Agent 1","user_role":"agent","user_state":"active","user_create":"2016-02-01 02:00:00"},
                                            {"user_id":"4","user_login":"agent2","user_pass":"9f511c88796e8fdd5b8e6aa832028ae7bd76ddfa","auth_key":"dpLRQ70i6vkeSTjTtN7rkcUbaxhjM03N","password_reset_token":"","user_nick":"Agent 2","user_role":"agent","user_state":"active","user_create":"2016-02-01 03:00:00"},
                                            {"user_id":"5","user_login":"agent3","user_pass":"fdb876f099ef3766cb1770f9ed5199a8b5d7aa57","auth_key":"eQ5bRhATtZjfwxD8PcCymrPFVR4sTTSb","password_reset_token":"","user_nick":"Agent 3","user_role":"agent","user_state":"active","user_create":"2016-02-01 04:00:00"},
                                            {"user_id":"8","user_login":"qwerty@mail.ru","user_pass":"16a4fb6d357cea5a58b4e7a466986ee586fa13b8","auth_key":"xTb7vqkLdOUi926ownBf1rfUIPBY4qLy","password_reset_token":"","user_nick":"qwerty","user_role":"agent","user_state":"active","user_create":"2016-02-26 00:45:28"}
                                        ]
                                    };
                    var groupdata = {
                                        "status": true,
                                        "list": [
                                           {"group_id": "3","group_title": "Test1","group_create": "2016-02-23 09:01:11"},
                                           {"group_id": "4","group_title": "Test2","group_create": "2016-02-23 09:01:20"},
                                           {"group_id": "5","group_title": "Test3","group_create": "2016-02-23 09:01:32"}
                                        ]
                                    };
                    $scope.all_agents = agentdata.list;
                    $scope.all_groups = groupdata.list;
                    var emotiondata =   {
                                           "status":true,
                                           "list":[
                                        
                                               {"emotion_id":"1","emotion_key":"angry","emotion_title":"Angry","emotion_url":"tmon_angry.png","emotion_state":"active","emotion_create":"2016-03-23 10:59:37"},
                                               {"emotion_id":"2","emotion_key":"asking","emotion_title":"Asking","emotion_url":"tmon_ask.png","emotion_state":"active","emotion_create":"2016-03-23 11:00:03"},
                                               {"emotion_id":"3","emotion_key":"caprise","emotion_title":"Caprise","emotion_url":"tmon_caprise.png","emotion_state":"active","emotion_create":"2016-03-23 11:00:23"},
                                               {"emotion_id":"4","emotion_key":"cry","emotion_title":"Cry","emotion_url":"tmon_crying.png","emotion_state":"active","emotion_create":"2016-03-23 11:00:40"},
                                               {"emotion_id":"5","emotion_key":"delivery","emotion_title":"Delivery","emotion_url":"tmon_getting.png","emotion_state":"active","emotion_create":"2016-03-23 11:01:02"},
                                               {"emotion_id":"6","emotion_key":"good","emotion_title":"Good","emotion_url":"tmon_okay.png","emotion_state":"active","emotion_create":"2016-03-23 11:01:18"},
                                               {"emotion_id":"7","emotion_key":"happy","emotion_title":"Happy","emotion_url":"tmon_happy.png","emotion_state":"active","emotion_create":"2016-03-23 11:01:40"},
                                               {"emotion_id":"8","emotion_key":"ice","emotion_title":"Ice","emotion_url":"tmon_ice.png","emotion_state":"active","emotion_create":"2016-03-23 11:01:57"},
                                               {"emotion_id":"9","emotion_key":"inlove","emotion_title":"Inlove","emotion_url":"tmon_love.png","emotion_state":"active","emotion_create":"2016-03-23 11:29:32"},
                                               {"emotion_id":"10","emotion_key":"shock","emotion_title":"Shock","emotion_url":"tmon_wondering.png","emotion_state":"active","emotion_create":"2016-03-23 11:29:59"},
                                               {"emotion_id":"11","emotion_key":"shopping","emotion_title":"Shopping","emotion_url":"tmon_shopping.png","emotion_state":"active","emotion_create":"2016-03-23 11:30:17"},
                                               {"emotion_id":"12","emotion_key":"snow","emotion_title":"Snow","emotion_url":"tmon_snow.png","emotion_state":"active","emotion_create":"2016-03-23 11:30:34"},
                                               {"emotion_id":"13","emotion_key":"sorry","emotion_title":"Sorry","emotion_url":"tmon_sorry.png","emotion_state":"active","emotion_create":"2016-03-23 11:30:51"},
                                               {"emotion_id":"14","emotion_key":"speachless","emotion_title":"Speachless","emotion_url":"tmon_dissappointed.png","emotion_state":"active","emotion_create":"2016-03-23 11:31:12"},
                                               {"emotion_id":"15","emotion_key":"typing","emotion_title":"Typing","emotion_url":"tmon_texting.png","emotion_state":"active","emotion_create":"2016-03-23 11:31:38"},
                                               {"emotion_id":"16","emotion_key":"yes","emotion_title":"Yes","emotion_url":"tmon_yes.png","emotion_state":"active","emotion_create":"2016-03-23 11:31:52"}
                                           ]
                                        };
                    $scope.all_emotions = emotiondata.list;
                } else{
    
                    tmonServices.getAgents()
                            .then(function(data){
                                console.log('[Get All Agents]', data);
                                $scope.all_agents = data.list;
                            })
                            .catch(function(error){
                                console.log('[Get All Agents Error]', error);
                            });
                    tmonServices.getGroups()
                        .then(function(data){
                            console.log('[Get All Groups]', data);
                            $scope.all_groups = data.list;
                        })
                        .catch(function(error){
                            console.log('[Get All Groups Error]', error);
                        });
    
    
                    tmonServices.getEmotions()
                        .then(function(data){
                            console.log('list all emotions: ', data);
                            $scope.all_emotions = data.list;
                        })
                        .catch(function(error){
                            console.log('list all emotions error', error);
                        });
                }
                $scope.getAllChats();
                //$scope.getEmptyChats();
                //$scope.getGroupChats();
                //$scope.getMeChats();
            }
           onStart();
        }
    }());
    //Filters
    (function(){
        'use strict';
        tmon.filter('reverse', function(){
            return function(items){
                return items.slice().reverse();
            }
        });
    }());
    //Services
    (function(){
        'use strict';
    
        tmon.factory('tmonServices', ['$http', '$q', '$timeout', 'uuid4', 'tmonSocket',tmonServices]);
        function tmonServices($http,$q,$timeout,uuid4,tmonSocket){
    
            function genuuid(txt){
                return uuid4.generate(txt);
            }
            function getProduct(pid){
                console.log('pid', pid);
                var deferred = $q.defer();
                $http.get("index.php?r=products/item&product_id="+pid)
                    .success(function(data){
                    deferred.resolve(data);
                }).error(function(error){
                    deferred.reject(error);
                });
                return deferred.promise;
            }
            function getOrder(oid){
                console.log('oid', oid);
                var deferred = $q.defer();
                $http.get("index.php?r=orders/item&order_id="+oid
                ).success(function(data){
                    deferred.resolve(data);
                }).error(function(error){
                    deferred.reject(error);
                });
                return deferred.promise;
            }
            function getInfo(chat){
                console.log('prod id getInfo', chat.product_id);
                console.log('order id getInfo', chat.order_id);
                if(chat.product_id == 0){
                    console.log('getting info by orderId');
                    console.log('order id getInfo', chat.order_id);
                    return getOrder(chat.order_id);
                }else{
                    console.log('getting info by productId');
                    console.log('prod id getInfo', chat.product_id);
                    return getProduct(chat.product_id);
                }
            }
            function getPurchases(user_id){
                var deferred = $q.defer();
                $http.get("index.php?r=orders/item&user_id="+user_id, {
                    cache: true
                }).success(function(data){
                    deferred.resolve(data);
                }).error(function(error){
                    deferred.reject(error);
                });
                return deferred.promise;
            }
            function getAgents(){
                var deferred = $q.defer();
                var host = window.location.hostname == 'localhost' ? 'http://tmon.mss.uz/' : '';
                $http.get(host+"index.php?r=api/agents-list&id="+agent_id+"&token="+user_token, {
                    cache: true
                }).success(function(data){
                    deferred.resolve(data);
                }).error(function(error){
                    deferred.reject(error);
                });
                return deferred.promise;
            }
            function getGroups(){
                var deferred = $q.defer();
                var host = window.location.hostname == 'localhost' ? 'http://tmon.mss.uz/' : '';
                $http.get(host+"index.php?r=api/groups-list&id="+agent_id+"&token="+user_token, {
                    cache: true
                }).success(function(data){
                    deferred.resolve(data);
                }).error(function(error){
                    deferred.reject(error);
                });
                return deferred.promise;
            }
            function getEmotions(){
                var deferred = $q.defer();
                var host = window.location.hostname == 'localhost' ? 'http://tmon.mss.uz/' : '';
                $http.get(host+"index.php?r=api/list-emotions&id="+agent_id+"&token="+user_token, {
                    cache: true
                }).success(function(data){
                    deferred.resolve(data);
                }).error(function(error){
                    deferred.reject(error);
                });
                return deferred.promise;
            }
            function styleTyping(data){
                var $class = $('.userId_'+ data.user_id),
                    state = data.chatrelation_state;
                if(state == "typing"){
                    $class.addClass('typing');
                }else{
                    $class.removeClass('typing');
                }
            }
            function scrollBottom(){
                var $txtbody = $(".textbox-body");
                $txtbody.animate({scrollTop: $txtbody.prop("scrollHeight")},500);
            }
            function pushHistory(data, history_msg, last_msg_date){
                var msgs = data.later;
                if(!msgs.length){
                    history_msg = false;
                }else{
                    var new_last_date = data.later[data.later.length-1].message_create;
                    if(last_msg_date == new_last_date){
                        $('.textbox-'+data.chat_id+' .history-icon').addClass('dn');
                    }else{
                        msgs.map(function(msg){
                            history_msg.push(msg);
                        });
                        return data.later[data.later.length-1].message_create;
                    }
                }
            }
            function sendTyping(typing, chat_id, agent_id){
                if(!typing){
                    typing = true; console.log('true typing');
                    tmonSocket.emit('user_relation_v3', {chat_id: chat_id, user_id: agent_id, chatrelation_state:'typing'});
                    $timeout(function(){
                        typing = false;
                        tmonSocket.emit('user_relation_v3', {chat_id: chat_id, user_id: agent_id, chatrelation_state:'active'});
                    },5000);
                }
            }
            function fwAgent(userid, chatid){
                var data = {chat_id: chatid, user_id :userid, chatrelation_role: 'manager', chatrelation_state: 'active'};
                console.log('dataaaa',data);
                tmonSocket.emit('user_relation_v3', data);
            }
            function fwGroup(groupid, chatid){
                var data = {chat_id: chatid, group_id: groupid, chatrelation_role: 'member', chatrelation_state: 'active'};
                
                tmonSocket.emit('user_relation_v3', data);
            }
            function becomeMember(chatid){
                var data = {chat_id: chatid, user_id: agent_id, chatrelation_role: 'member', chatrelation_state: 'active'};
                
                tmonSocket.emit('user_relation_v3', data);
            }
            function messageDelivery(msgid){
                console.log('msgid');
                tmonSocket.emit('message_delivery_v3', {message_id: msgid});
            }
    
            function getUrlInfo(url){
                var deferred = $q.defer();
                if (window.location.hostname == 'localhost') {
                    deferred.resolve({
                            "status": true,
                            "title": "Mail.Ru: почта, поиск в интернете, новости, игры",
                            "metas": {
                                "og:url": "https://mail.ru",
                                "og:type": "website",
                                "og:title": "Mail.Ru: почта, поиск в интернете, новости, игры",
                                "og:image": "https://limg.imgsmail.ru/splash/v/i/share.8b203538c26d6c0bf050e43faf4b03712e7feb0d.png",
                                "og:description": "Mail.Ru — крупнейшая бесплатная почта, быстрый и удобный интерфейс, неограниченный объем ящика, надежная защита от спама и вирусов, мобильная версия и приложения для смартфонов. Также на Mail.Ru: новости, поиск в интернете, игры, авто, спорт, знакомства, погода, работа"
                            }
                            })
                } else{
                    $http.get("index.php?r=api/parse&url="+url, {
                        cache: true
                    }).success(function(data){
                        deferred.resolve(data);
                    }).error(function(error){
                        deferred.reject(error);
                    });
                }
                return deferred.promise;
            }
    
            return {
                genuuid: genuuid,
                getInfo: getInfo,
                getProduct: getProduct,
                getOrder: getOrder,
                getAgents: getAgents,
                getGroups: getGroups,
                styleTyping: styleTyping,
                scrollBottom: scrollBottom,
                pushHistory: pushHistory,
                sendTyping: sendTyping,
                fwAgent: fwAgent,
                fwGroup: fwGroup,
                becomeMember: becomeMember,
                messageDelivery: messageDelivery,
                getEmotions:getEmotions,
                getUrlInfo: getUrlInfo
            }
        }
    }());
    (function(){
        'use strict';
    
        tmon.factory('tmonSocket', ['$rootScope', 'socketFactory', tmonSocket]);
        function tmonSocket($rootScope, socketFactory){
            
            var myIoSocket = window.location.hostname == 'localhost' ? 
                            io.connect('http://tmon.mss.uz:85/chat?user_id='+agent_id+'&user_token=-1') : 
                            io.connect(':85/chat?user_id='+agent_id+'&user_token='+user_token);
    
            
            tmonSocket = socketFactory({
                ioSocket: myIoSocket
            });
            tmonSocket.on('connect', function(){
                console.log('connected');
            });
            tmonSocket.on('disconnect', function(){
                console.log('disconnect');
            });
            tmonSocket.forward('all_chats_v3', $rootScope); 
            tmonSocket.forward('user_chats_v3', $rootScope);
            tmonSocket.forward('group_chats_v3', $rootScope);
            tmonSocket.forward('empty_chats_v3', $rootScope);
            tmonSocket.forward('relation_v3', $rootScope);
            tmonSocket.forward('chat_history_v3', $rootScope);
            tmonSocket.forward('chat_relations_v3', $rootScope);
            tmonSocket.forward('message_v3', $rootScope);
            tmonSocket.forward('assign_manager_v3', $rootScope);
            tmonSocket.forward('user_info_v3', $rootScope);
            tmonSocket.forward('message_delivery_v3', $rootScope);
            return tmonSocket;
        }
    }());

}());

function rotate(selector) {
    $(selector).animate({
        left: $('.load').width()
    }, 1000, function() {
        $(selector).css("left", -($(selector).width()) + "px");
        rotate(selector);
    });
}
/**
 * Created by Ayo on 08-Feb-16.
 */
//var socket = io.connect('http://localhost:3000/');